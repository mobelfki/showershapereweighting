
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
void write3HistosRatio (TString InputPath, TString Name, std::string outFileName) {
    
    TString firstInputFile = TString (InputPath)+"data_Zlly_With_Zero.root";
    TString secondInputFile = TString (InputPath)+"mc_Zlly_With_Zero.root";	
    TString thirdInputFile = TString (InputPath)+"mc_Zlly_With_Zero_Rew.root";

    TFile *file1 = new TFile(firstInputFile);
    TFile *file2 = new TFile(secondInputFile);	
    TFile *file3 = new TFile(thirdInputFile);

    TString firstHistName = TString (Name)+" MC ";
    TString secondHistName = TString (Name)+ " Data  ";  	

    TString HistName = TString (Name);
    TString RewHistName = TString (Name);
    
    TH1F* dataHist = (TH1F*) file1->Get(HistName);
    TH1F* mcRef = (TH1F*) file2->Get(HistName); 
    TH1F* mcRew = (TH1F*) file3->Get(HistName);  
    
    TCanvas *canv = new TCanvas(RewHistName, RewHistName,900,700);
    TH1F * ratioHist1;
    TH1F * ratioHist2;

    //dataHist->Scale((mcRef->Integral())/(dataHist->Integral()));
    //mcRew->Scale((mcRef->Integral())/(mcRew->Integral()));
    
    canv->cd();
    TPad *up = new TPad ("pad1","pad1",0,0.3,1,1.0);
    up->Draw();
    up->cd();
    

    TLegend *legend=new TLegend(0.78,0.57,0.98,0.77);
    
    
    legend->SetTextFont(72);
    legend->SetTextSize(0.04);
    legend->AddEntry(dataHist,"data");
    legend->AddEntry(mcRef,"Nominal MC");
    legend->AddEntry(mcRew,"Reweighted MC");
    
    mcRef->SetStats(0);
    dataHist->SetStats(0);
    mcRew->SetStats(0);

    mcRef->SetLineColor(2);
    mcRef->SetLineWidth(2);
	
    TString axisTitle(Name(0,5));

    mcRef->GetXaxis()->SetLabelFont(62);
    mcRef->GetYaxis()->SetLabelFont(62);
    mcRef->GetXaxis()->SetTitle(axisTitle);
    mcRef->GetXaxis()->SetLabelSize(0.045);
    mcRef->GetYaxis()->SetLabelSize(0.045);
    mcRef->GetYaxis()->SetTitle("A.U");

    mcRef->Draw();
    dataHist->SetLineColor(1);
    dataHist->SetMarkerStyle(20);
    dataHist->Draw("same");
    mcRew->SetLineColor(4);
    mcRew->SetLineWidth(2);
    mcRew->Draw("same");
    
    ratioHist1 = (TH1F*) mcRef->Clone();
    ratioHist2 = (TH1F*) mcRew->Clone();
    
    TPad *down = new TPad ("pad2","pad2",0,0.05,1,0.3);
    
    legend->Draw();
    
    canv->cd();
    
    down->Draw();
    down->cd();
    ratioHist1->Divide(dataHist);
    ratioHist1->GetYaxis()->SetLabelSize(0.08);
    //ratioHist1->GetYaxis()->SetRangeUser(0.85,1.1);
    ratioHist1->SetStats(0);
    ratioHist1->SetLineColor(2);
    ratioHist1->SetLineWidth(2);
    ratioHist1->Draw();
    
    ratioHist2->Divide(dataHist);
    //ratioHist2->GetYaxis()->SetLabelSize(0.08);
    //ratioHist2->GetYaxis()->SetRangeUser(0.85,1.1);
    ratioHist2->SetStats(0);
    ratioHist2->SetLineColor(4);
    ratioHist2->SetLineWidth(2);
    ratioHist2->Draw("same");

    double xmax = mcRef->GetXaxis()->GetXmax();
    double xmin = mcRef->GetXaxis()->GetXmin();

    TLine *line = new TLine(xmin,1.,xmax,1.);
    line->SetLineColor(6);
    line->SetLineWidth(2);
    line->SetLineStyle(2);
    line->Draw("same");
    
    canv->Print(Form("/afs/cern.ch/user/m/mobelfki/Test_athena/NTUP-ANALYSIS/Test_RadiativeZ/Plots/%s.root", outFileName.c_str()));
    canv->~TCanvas();
}

