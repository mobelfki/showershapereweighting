//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Apr  5 09:32:51 2019 by ROOT version 6.12/06
// from TTree NTUP/Zlly Ntuple
// found on file: data2017.root
//////////////////////////////////////////////////////////

#ifndef NTUP_h
#define NTUP_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
TString coef_file = "/afs/cern.ch/user/m/mobelfki/Test_athena/NTUP-ANALYSIS/Output/alldata_NCon.root";
//TString coef_file = "/afs/cern.ch/user/m/mobelfki/EgammaRew/source/athena/PhysicsAnalysis/DerivationFramework/DerivationFrameworkCalo/rewCoeffs10.root";
TFile *Coeff = new TFile(coef_file);

				TString data = "/eos/user/m/mobelfki/ZllyAthDerivation2/data2017.root";
				TString mc = "/eos/user/m/mobelfki/ZllyAthDerivation2/mc.root";
				TString mc_withPilePU = "/eos/user/m/mobelfki/ZllyAthDerivation2/mc/mc.root";
				TString data2 = "/eos/user/m/mobelfki/ZllyAthDerivation/data/data.root";
				TString mc2 = "/eos/user/m/mobelfki/ZllyAthDerivation/mc/mca+d.root";
				TString mc_withoutPilePU = "/eos/user/m/mobelfki/ZllyAthDerivation/mc/mc.root";
				TString rew = "/eos/user/m/mobelfki/ZllyAthDerivation/mc/mc_rew.root";
				TString myrew = "/eos/user/m/mobelfki/ZllyAthDerivation/mc/mc_myrew.root";
				
				//TString sample = data;  TString output = "Output/data_wgt.root";
				TString sample = mc;      TString output = "Output/mc_wgt_Rew_allRew.root";
				//TString sample = rew;     TString output = "Output/mc_rew.root";
				//TString sample = myrew;     TString output = "Output/mc_myrew.root";

				 // TString sample = mc_withPilePU; TString output = "Output/mc_withPilePU.root";
				// TString sample = mc_withoutPilePU; TString output = "Output/mc_withoutPilePU.root";

class NTUP {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   float Energy(int eta, int phi, vector<float> *clusterEnergy);
   float Energy2(int eta, int phi, float clusterEnergy[77]);
   float calcWeta (vector<float> *clusterEnergy, vector<float> *clusterEta);
   float calcWeta2 (float clusterEnergy[77], vector<float> *clusterEta);
   // Declaration of leaf types
   Double_t        ph_Pt;
   Double_t        ph_Eta;
   Double_t        ph_Phi;
   Double_t        ph_E;
   Double_t        ph_topoetcone20;
   Double_t        ph_topoetcone30;
   Double_t        ph_topoetcone40;
   Double_t        ph_ptcone20;
   Double_t        ph_ptcone30;
   Double_t        ph_ptcone40;
   Double_t        ph_etcone20;
   Double_t        ph_etcone30;
   Double_t        ph_etcone40;
   Double_t        ph_ptvarcone20;
   Double_t        ph_ptvarcone30;
   Double_t        ph_ptvarcone40;
   Bool_t          ph_isIsoLooseWP;
   Bool_t          ph_isIsoTightWP;
   Bool_t          ph_isIsoTightCaloOnlyWP;
   Double_t        ph_Conversion;
   Double_t        ph_Ethad;
   Double_t        ph_Ethad1;
   Double_t        ph_Rhad1;
   Double_t        ph_Rhad;
   Double_t        ph_E011;
   Double_t        ph_E132;
   Double_t        ph_E237;
   Double_t        ph_E277;
   Double_t        ph_Reta;
   Double_t        ph_Rphi;
   Double_t        ph_Weta1;
   Double_t        ph_Weta2;
   Double_t        ph_f1;
   Double_t        ph_f3;
   Double_t        ph_f3core;
   Double_t        ph_fracs1;
   Double_t        ph_Wstot1;
   Double_t        ph_deltaE;
   Double_t        ph_Eratio;
   Double_t        ph_E2tsts1;
   Double_t        ph_Emins1;
   Double_t        ph_Emaxs1;
   Int_t           ph_ClusterSize7x11Lr2;
   Int_t           ph_ClusterSize3x7Lr2;
   Int_t           ph_ClusterSize5x5Lr2;
   Int_t           ph_ClusterSize7x11Lr3;
   Int_t           ph_ClusterSize3x7Lr3;
   Int_t           ph_ClusterSize5x5Lr3;
   Int_t           ph_ClusterSize7x11Lr1;
   Int_t           ph_ClusterSize3x7Lr1;
   Int_t           ph_ClusterSize5x5Lr1;
   vector<float>   *ph_clusterCellsLr2E7x11;
   vector<float>   *ph_clusterCellsLr2Eta7x11;
   vector<float>   *ph_clusterCellsLr2Phi7x11;
   vector<float>   *ph_clusterCellsLr2E3x7;
   vector<float>   *ph_clusterCellsLr2Eta3x7;
   vector<float>   *ph_clusterCellsLr2Phi3x7;
   vector<float>   *ph_clusterCellsLr2E5x5;
   vector<float>   *ph_clusterCellsLr2Eta5x5;
   vector<float>   *ph_clusterCellsLr2Phi5x5;
   vector<float>   *ph_clusterCellsLr1E7x11;
   vector<float>   *ph_clusterCellsLr1Eta7x11;
   vector<float>   *ph_clusterCellsLr1Phi7x11;
   vector<float>   *ph_clusterCellsLr1E3x7;
   vector<float>   *ph_clusterCellsLr1Eta3x7;
   vector<float>   *ph_clusterCellsLr1Phi3x7;
   vector<float>   *ph_clusterCellsLr1E5x5;
   vector<float>   *ph_clusterCellsLr1Eta5x5;
   vector<float>   *ph_clusterCellsLr1Phi5x5;
   vector<float>   *ph_clusterCellsLr3E7x11;
   vector<float>   *ph_clusterCellsLr3Eta7x11;
   vector<float>   *ph_clusterCellsLr3Phi7x11;
   vector<float>   *ph_clusterCellsLr3E3x7;
   vector<float>   *ph_clusterCellsLr3Eta3x7;
   vector<float>   *ph_clusterCellsLr3Phi3x7;
   vector<float>   *ph_clusterCellsLr3E5x5;
   vector<float>   *ph_clusterCellsLr3Eta5x5;
   vector<float>   *ph_clusterCellsLr3Phi5x5;
   Double_t        l1_Pt;
   Double_t        l1_Eta;
   Double_t        l1_Phi;
   Double_t        l1_E;
   Double_t        l1_Charge;
   Double_t        l2_Pt;
   Double_t        l2_Eta;
   Double_t        l2_Phi;
   Double_t        l2_E;
   Double_t        l2_Charge;
   Double_t        l1_SF;
   Double_t        l2_SF;
   Double_t        pu_wgt;
   Double_t        Mu;
   Double_t        mc_wgt;
   Double_t        mc_xsec;
   Bool_t          isZeey;
   Bool_t          isZmumuy;
   vector<float>   *M_ee;
   vector<float>   *M_eey;
   vector<float>   *M_mumu;
   vector<float>   *M_mumuy;

   // List of branches
   TBranch        *b_ph_Pt;   //!
   TBranch        *b_ph_Eta;   //!
   TBranch        *b_ph_Phi;   //!
   TBranch        *b_ph_E;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_topoetcone30;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_ptcone30;   //!
   TBranch        *b_ph_ptcone40;   //!
   TBranch        *b_ph_etcone20;   //!
   TBranch        *b_ph_etcone30;   //!
   TBranch        *b_ph_etcone40;   //!
   TBranch        *b_ph_ptvarcone20;   //!
   TBranch        *b_ph_ptvarcone30;   //!
   TBranch        *b_ph_ptvarcone40;   //!
   TBranch        *b_ph_isIsoLooseWP;   //!
   TBranch        *b_ph_isIsoTightWP;   //!
   TBranch        *b_ph_isIsoTightCaloOnlyWP;   //!
   TBranch        *b_ph_Conversion;   //!
   TBranch        *b_ph_Ethad;   //!
   TBranch        *b_ph_Ethad1;   //!
   TBranch        *b_ph_Rhad1;   //!
   TBranch        *b_ph_Rhad;   //!
   TBranch        *b_ph_E011;   //!
   TBranch        *b_ph_E132;   //!
   TBranch        *b_ph_E237;   //!
   TBranch        *b_ph_E277;   //!
   TBranch        *b_ph_Reta;   //!
   TBranch        *b_ph_Rphi;   //!
   TBranch        *b_ph_Weta1;   //!
   TBranch        *b_ph_Weta2;   //!
   TBranch        *b_ph_f1;   //!
   TBranch        *b_ph_f3;   //!
   TBranch        *b_ph_f3core;   //!
   TBranch        *b_ph_fracs1;   //!
   TBranch        *b_ph_Wstot1;   //!
   TBranch        *b_ph_deltaE;   //!
   TBranch        *b_ph_Eratio;   //!
   TBranch        *b_ph_E2tsts1;   //!
   TBranch        *b_ph_Emins1;   //!
   TBranch        *b_ph_Emaxs1;   //!
   TBranch        *b_ph_ClusterSize7x11Lr2;   //!
   TBranch        *b_ph_ClusterSize3x7Lr2;   //!
   TBranch        *b_ph_ClusterSize5x5Lr2;   //!
   TBranch        *b_ph_ClusterSize7x11Lr3;   //!
   TBranch        *b_ph_ClusterSize3x7Lr3;   //!
   TBranch        *b_ph_ClusterSize5x5Lr3;   //!
   TBranch        *b_ph_ClusterSize7x11Lr1;   //!
   TBranch        *b_ph_ClusterSize3x7Lr1;   //!
   TBranch        *b_ph_ClusterSize5x5Lr1;   //!
   TBranch        *b_ph_clusterCellsLr2E7x11;   //!
   TBranch        *b_ph_clusterCellsLr2Eta7x11;   //!
   TBranch        *b_ph_clusterCellsLr2Phi7x11;   //!
   TBranch        *b_ph_clusterCellsLr2E3x7;   //!
   TBranch        *b_ph_clusterCellsLr2Eta3x7;   //!
   TBranch        *b_ph_clusterCellsLr2Phi3x7;   //!
   TBranch        *b_ph_clusterCellsLr2E5x5;   //!
   TBranch        *b_ph_clusterCellsLr2Eta5x5;   //!
   TBranch        *b_ph_clusterCellsLr2Phi5x5;   //!
   TBranch        *b_ph_clusterCellsLr1E7x11;   //!
   TBranch        *b_ph_clusterCellsLr1Eta7x11;   //!
   TBranch        *b_ph_clusterCellsLr1Phi7x11;   //!
   TBranch        *b_ph_clusterCellsLr1E3x7;   //!
   TBranch        *b_ph_clusterCellsLr1Eta3x7;   //!
   TBranch        *b_ph_clusterCellsLr1Phi3x7;   //!
   TBranch        *b_ph_clusterCellsLr1E5x5;   //!
   TBranch        *b_ph_clusterCellsLr1Eta5x5;   //!
   TBranch        *b_ph_clusterCellsLr1Phi5x5;   //!
   TBranch        *b_ph_clusterCellsLr3E7x11;   //!
   TBranch        *b_ph_clusterCellsLr3Eta7x11;   //!
   TBranch        *b_ph_clusterCellsLr3Phi7x11;   //!
   TBranch        *b_ph_clusterCellsLr3E3x7;   //!
   TBranch        *b_ph_clusterCellsLr3Eta3x7;   //!
   TBranch        *b_ph_clusterCellsLr3Phi3x7;   //!
   TBranch        *b_ph_clusterCellsLr3E5x5;   //!
   TBranch        *b_ph_clusterCellsLr3Eta5x5;   //!
   TBranch        *b_ph_clusterCellsLr3Phi5x5;   //!
   TBranch        *b_l1_Pt;   //!
   TBranch        *b_l1_Eta;   //!
   TBranch        *b_l1_Phi;   //!
   TBranch        *b_l1_E;   //!
   TBranch        *b_l1_Charge;   //!
   TBranch        *b_l2_Pt;   //!
   TBranch        *b_l2_Eta;   //!
   TBranch        *b_l2_Phi;   //!
   TBranch        *b_l2_E;   //!
   TBranch        *b_l2_Charge;   //!
   TBranch        *b_l1_SF;   //!
   TBranch        *b_l2_SF;   //!
   TBranch        *b_pu_wgt;   //!
   TBranch        *b_Mu;   //!
   TBranch        *b_mc_wgt;   //!
   TBranch        *b_mc_xsec;   //!
   TBranch        *b_isZeey;   //!
   TBranch        *b_isZmumuy;   //!
   TBranch        *b_M_ee;   //!
   TBranch        *b_M_eey;   //!
   TBranch        *b_M_mumu;   //!
   TBranch        *b_M_mumuy;   //!

   NTUP(TTree *tree=0);
   virtual ~NTUP();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(bool doRew);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef NTUP_cxx
NTUP::NTUP(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(sample);
      if (!f || !f->IsOpen()) {
         f = new TFile(sample);
      }
      f->GetObject("NTUP",tree);

   }
   Init(tree);
}

NTUP::~NTUP()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t NTUP::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t NTUP::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void NTUP::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   ph_clusterCellsLr2E7x11 = 0;
   ph_clusterCellsLr2Eta7x11 = 0;
   ph_clusterCellsLr2Phi7x11 = 0;
   ph_clusterCellsLr2E3x7 = 0;
   ph_clusterCellsLr2Eta3x7 = 0;
   ph_clusterCellsLr2Phi3x7 = 0;
   ph_clusterCellsLr2E5x5 = 0;
   ph_clusterCellsLr2Eta5x5 = 0;
   ph_clusterCellsLr2Phi5x5 = 0;
   ph_clusterCellsLr1E7x11 = 0;
   ph_clusterCellsLr1Eta7x11 = 0;
   ph_clusterCellsLr1Phi7x11 = 0;
   ph_clusterCellsLr1E3x7 = 0;
   ph_clusterCellsLr1Eta3x7 = 0;
   ph_clusterCellsLr1Phi3x7 = 0;
   ph_clusterCellsLr1E5x5 = 0;
   ph_clusterCellsLr1Eta5x5 = 0;
   ph_clusterCellsLr1Phi5x5 = 0;
   ph_clusterCellsLr3E7x11 = 0;
   ph_clusterCellsLr3Eta7x11 = 0;
   ph_clusterCellsLr3Phi7x11 = 0;
   ph_clusterCellsLr3E3x7 = 0;
   ph_clusterCellsLr3Eta3x7 = 0;
   ph_clusterCellsLr3Phi3x7 = 0;
   ph_clusterCellsLr3E5x5 = 0;
   ph_clusterCellsLr3Eta5x5 = 0;
   ph_clusterCellsLr3Phi5x5 = 0;
   M_ee = 0;
   M_eey = 0;
   M_mumu = 0;
   M_mumuy = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("ph_Pt", &ph_Pt, &b_ph_Pt);
   fChain->SetBranchAddress("ph_Eta", &ph_Eta, &b_ph_Eta);
   fChain->SetBranchAddress("ph_Phi", &ph_Phi, &b_ph_Phi);
   fChain->SetBranchAddress("ph_E", &ph_E, &b_ph_E);
   fChain->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20, &b_ph_topoetcone20);
   fChain->SetBranchAddress("ph_topoetcone30", &ph_topoetcone30, &b_ph_topoetcone30);
   fChain->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40, &b_ph_topoetcone40);
   fChain->SetBranchAddress("ph_ptcone20", &ph_ptcone20, &b_ph_ptcone20);
   fChain->SetBranchAddress("ph_ptcone30", &ph_ptcone30, &b_ph_ptcone30);
   fChain->SetBranchAddress("ph_ptcone40", &ph_ptcone40, &b_ph_ptcone40);
   fChain->SetBranchAddress("ph_etcone20", &ph_etcone20, &b_ph_etcone20);
   fChain->SetBranchAddress("ph_etcone30", &ph_etcone30, &b_ph_etcone30);
   fChain->SetBranchAddress("ph_etcone40", &ph_etcone40, &b_ph_etcone40);
   fChain->SetBranchAddress("ph_ptvarcone20", &ph_ptvarcone20, &b_ph_ptvarcone20);
   fChain->SetBranchAddress("ph_ptvarcone30", &ph_ptvarcone30, &b_ph_ptvarcone30);
   fChain->SetBranchAddress("ph_ptvarcone40", &ph_ptvarcone40, &b_ph_ptvarcone40);
   fChain->SetBranchAddress("ph_isIsoLooseWP", &ph_isIsoLooseWP, &b_ph_isIsoLooseWP);
   fChain->SetBranchAddress("ph_isIsoTightWP", &ph_isIsoTightWP, &b_ph_isIsoTightWP);
   fChain->SetBranchAddress("ph_isIsoTightCaloOnlyWP", &ph_isIsoTightCaloOnlyWP, &b_ph_isIsoTightCaloOnlyWP);
   fChain->SetBranchAddress("ph_Conversion", &ph_Conversion, &b_ph_Conversion);
   fChain->SetBranchAddress("ph_Ethad", &ph_Ethad, &b_ph_Ethad);
   fChain->SetBranchAddress("ph_Ethad1", &ph_Ethad1, &b_ph_Ethad1);
   fChain->SetBranchAddress("ph_Rhad1", &ph_Rhad1, &b_ph_Rhad1);
   fChain->SetBranchAddress("ph_Rhad", &ph_Rhad, &b_ph_Rhad);
   fChain->SetBranchAddress("ph_E011", &ph_E011, &b_ph_E011);
   fChain->SetBranchAddress("ph_E132", &ph_E132, &b_ph_E132);
   fChain->SetBranchAddress("ph_E237", &ph_E237, &b_ph_E237);
   fChain->SetBranchAddress("ph_E277", &ph_E277, &b_ph_E277);
   fChain->SetBranchAddress("ph_Reta", &ph_Reta, &b_ph_Reta);
   fChain->SetBranchAddress("ph_Rphi", &ph_Rphi, &b_ph_Rphi);
   fChain->SetBranchAddress("ph_Weta1", &ph_Weta1, &b_ph_Weta1);
   fChain->SetBranchAddress("ph_Weta2", &ph_Weta2, &b_ph_Weta2);
   fChain->SetBranchAddress("ph_f1", &ph_f1, &b_ph_f1);
   fChain->SetBranchAddress("ph_f3", &ph_f3, &b_ph_f3);
   fChain->SetBranchAddress("ph_f3core", &ph_f3core, &b_ph_f3core);
   fChain->SetBranchAddress("ph_fracs1", &ph_fracs1, &b_ph_fracs1);
   fChain->SetBranchAddress("ph_Wstot1", &ph_Wstot1, &b_ph_Wstot1);
   fChain->SetBranchAddress("ph_deltaE", &ph_deltaE, &b_ph_deltaE);
   fChain->SetBranchAddress("ph_Eratio", &ph_Eratio, &b_ph_Eratio);
   fChain->SetBranchAddress("ph_E2tsts1", &ph_E2tsts1, &b_ph_E2tsts1);
   fChain->SetBranchAddress("ph_Emins1", &ph_Emins1, &b_ph_Emins1);
   fChain->SetBranchAddress("ph_Emaxs1", &ph_Emaxs1, &b_ph_Emaxs1);
   fChain->SetBranchAddress("ph_ClusterSize7x11Lr2", &ph_ClusterSize7x11Lr2, &b_ph_ClusterSize7x11Lr2);
   fChain->SetBranchAddress("ph_ClusterSize3x7Lr2", &ph_ClusterSize3x7Lr2, &b_ph_ClusterSize3x7Lr2);
   fChain->SetBranchAddress("ph_ClusterSize5x5Lr2", &ph_ClusterSize5x5Lr2, &b_ph_ClusterSize5x5Lr2);
   fChain->SetBranchAddress("ph_ClusterSize7x11Lr3", &ph_ClusterSize7x11Lr3, &b_ph_ClusterSize7x11Lr3);
   fChain->SetBranchAddress("ph_ClusterSize3x7Lr3", &ph_ClusterSize3x7Lr3, &b_ph_ClusterSize3x7Lr3);
   fChain->SetBranchAddress("ph_ClusterSize5x5Lr3", &ph_ClusterSize5x5Lr3, &b_ph_ClusterSize5x5Lr3);
   fChain->SetBranchAddress("ph_ClusterSize7x11Lr1", &ph_ClusterSize7x11Lr1, &b_ph_ClusterSize7x11Lr1);
   fChain->SetBranchAddress("ph_ClusterSize3x7Lr1", &ph_ClusterSize3x7Lr1, &b_ph_ClusterSize3x7Lr1);
   fChain->SetBranchAddress("ph_ClusterSize5x5Lr1", &ph_ClusterSize5x5Lr1, &b_ph_ClusterSize5x5Lr1);
   fChain->SetBranchAddress("ph_clusterCellsLr2E7x11", &ph_clusterCellsLr2E7x11, &b_ph_clusterCellsLr2E7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr2Eta7x11", &ph_clusterCellsLr2Eta7x11, &b_ph_clusterCellsLr2Eta7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr2Phi7x11", &ph_clusterCellsLr2Phi7x11, &b_ph_clusterCellsLr2Phi7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr2E3x7", &ph_clusterCellsLr2E3x7, &b_ph_clusterCellsLr2E3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr2Eta3x7", &ph_clusterCellsLr2Eta3x7, &b_ph_clusterCellsLr2Eta3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr2Phi3x7", &ph_clusterCellsLr2Phi3x7, &b_ph_clusterCellsLr2Phi3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr2E5x5", &ph_clusterCellsLr2E5x5, &b_ph_clusterCellsLr2E5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr2Eta5x5", &ph_clusterCellsLr2Eta5x5, &b_ph_clusterCellsLr2Eta5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr2Phi5x5", &ph_clusterCellsLr2Phi5x5, &b_ph_clusterCellsLr2Phi5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr1E7x11", &ph_clusterCellsLr1E7x11, &b_ph_clusterCellsLr1E7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr1Eta7x11", &ph_clusterCellsLr1Eta7x11, &b_ph_clusterCellsLr1Eta7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr1Phi7x11", &ph_clusterCellsLr1Phi7x11, &b_ph_clusterCellsLr1Phi7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr1E3x7", &ph_clusterCellsLr1E3x7, &b_ph_clusterCellsLr1E3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr1Eta3x7", &ph_clusterCellsLr1Eta3x7, &b_ph_clusterCellsLr1Eta3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr1Phi3x7", &ph_clusterCellsLr1Phi3x7, &b_ph_clusterCellsLr1Phi3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr1E5x5", &ph_clusterCellsLr1E5x5, &b_ph_clusterCellsLr1E5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr1Eta5x5", &ph_clusterCellsLr1Eta5x5, &b_ph_clusterCellsLr1Eta5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr1Phi5x5", &ph_clusterCellsLr1Phi5x5, &b_ph_clusterCellsLr1Phi5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr3E7x11", &ph_clusterCellsLr3E7x11, &b_ph_clusterCellsLr3E7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr3Eta7x11", &ph_clusterCellsLr3Eta7x11, &b_ph_clusterCellsLr3Eta7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr3Phi7x11", &ph_clusterCellsLr3Phi7x11, &b_ph_clusterCellsLr3Phi7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr3E3x7", &ph_clusterCellsLr3E3x7, &b_ph_clusterCellsLr3E3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr3Eta3x7", &ph_clusterCellsLr3Eta3x7, &b_ph_clusterCellsLr3Eta3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr3Phi3x7", &ph_clusterCellsLr3Phi3x7, &b_ph_clusterCellsLr3Phi3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr3E5x5", &ph_clusterCellsLr3E5x5, &b_ph_clusterCellsLr3E5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr3Eta5x5", &ph_clusterCellsLr3Eta5x5, &b_ph_clusterCellsLr3Eta5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr3Phi5x5", &ph_clusterCellsLr3Phi5x5, &b_ph_clusterCellsLr3Phi5x5);
   fChain->SetBranchAddress("l1_Pt", &l1_Pt, &b_l1_Pt);
   fChain->SetBranchAddress("l1_Eta", &l1_Eta, &b_l1_Eta);
   fChain->SetBranchAddress("l1_Phi", &l1_Phi, &b_l1_Phi);
   fChain->SetBranchAddress("l1_E", &l1_E, &b_l1_E);
   fChain->SetBranchAddress("l1_Charge", &l1_Charge, &b_l1_Charge);
   fChain->SetBranchAddress("l2_Pt", &l2_Pt, &b_l2_Pt);
   fChain->SetBranchAddress("l2_Eta", &l2_Eta, &b_l2_Eta);
   fChain->SetBranchAddress("l2_Phi", &l2_Phi, &b_l2_Phi);
   fChain->SetBranchAddress("l2_E", &l2_E, &b_l2_E);
   fChain->SetBranchAddress("l2_Charge", &l2_Charge, &b_l2_Charge);
   fChain->SetBranchAddress("l1_SF", &l1_SF, &b_l1_SF);
   fChain->SetBranchAddress("l2_SF", &l2_SF, &b_l2_SF);
   fChain->SetBranchAddress("pu_wgt", &pu_wgt, &b_pu_wgt);
   fChain->SetBranchAddress("Mu", &Mu, &b_Mu);
   fChain->SetBranchAddress("mc_wgt", &mc_wgt, &b_mc_wgt);
   fChain->SetBranchAddress("mc_xsec", &mc_xsec, &b_mc_xsec);
   fChain->SetBranchAddress("isZeey", &isZeey, &b_isZeey);
   fChain->SetBranchAddress("isZmumuy", &isZmumuy, &b_isZmumuy);
   fChain->SetBranchAddress("M_ee", &M_ee, &b_M_ee);
   fChain->SetBranchAddress("M_eey", &M_eey, &b_M_eey);
   fChain->SetBranchAddress("M_mumu", &M_mumu, &b_M_mumu);
   fChain->SetBranchAddress("M_mumuy", &M_mumuy, &b_M_mumuy);
   Notify();
}

Bool_t NTUP::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void NTUP::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t NTUP::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef NTUP_cxx
