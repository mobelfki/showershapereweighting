#include "TChain.h"
#include "TH1.h"
#include "TTree.h"
#include "TSystemDirectory.h"
#include "TH2.h"
#include <iostream>
#include <string>
#include <fstream>
#include <TSystem.h>
#include <vector>
#include <TFile.h>
#include "TString.h"

#include "Datavar.h"
#include "MCvar.h"
using namespace std;

void Loop(TChain* MC_chain, TChain* Data_chain,TString type,bool doRew);
void Ana(TChain* MC, TChain* Data, TString target,bool doRew);
int NTUP();
void Fin(float lumi);
void Init(TString target,bool doRew);
void HistoInit();
void InitRew();
float calcWeta(std::vector<float> *clusterEnergy, std::vector<float> *clusterEta);
float Energy(int eta, int phi, std::vector<float> *clusterEnergy);
float getXsec(TChain* fChain);


void SetMatchedTreeBranches(TTree *tree);

    const int etaSize = 7;
    const int phiSize = 11;
    const int clusterSize = 77;
    int const nType = 3;
    int const nTarget = 1;
    std::string Type[nType] = {"Inc","NCon","Con"};
    std::string _target[3] = {"Zlly","Zeey","Zmumuy"}; //,"Zmumuy","Inc"};
    int const nEtaBins = 14;
    const float etaLimits [nEtaBins +1] = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.3, 1.37, 1.52, 1.6, 1.80, 2.0, 2.2, 2.4};
    int const nEBins = 14;
    const float EBins [nEBins +1] = {0, 0.5, 1, 1.5, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12.5, 15};


    TProfile* CellCorrection[77][nEtaBins][nType];
   
    TFile* _out;
    TFile* _crr;



    int MC_i;
    int Data_i;

void InitRew(TString target, TString type)
{
	/*
	TString name = "Rew.";
	name += target;
	name += ".";
	name += type;
	name += ".root";
	std::cout<< " The reweighting coeffiecient is : "<<name<<std::endl;
	TFile* Coeff = new TFile(name,"READ");
	TH2F* cellsDelta[nEtaBins];
	TH1F* CellsEta[nEtaBins];
	for(int n = 0; n<nEtaBins; n++) 
	{	//std::cout<<etaLimits[n]<<std::endl;
		//std::cout<<Form("cellsDelta_Eta_%1.2f_%1.2f",etaLimits[n], etaLimits[n+1])<<std::endl;
		cellsDelta[n] = (TH2F*) Coeff->Get(Form("cellsDelta_Eta_%1.2f_%1.2f",etaLimits[n], etaLimits[n+1]));
		CellsEta[n] = (TH1F*) Coeff->Get(Form("etaProfileDelta_Eta_%1.2f_%1.2f",etaLimits[n], etaLimits[n+1]));
	}

	for (int n = 0; n < nEtaBins; n++) {
        for ( int phi = 1; phi <= phiSize; phi ++ ) {
        for ( int eta = 1; eta <= etaSize; eta ++ ) {
			//std::cout<<eta<<phi<<std::endl;
                        cellCorrDelta[(phi-1)+phiSize*(eta-1)][n] = cellsDelta[n]->GetBinContent(phi,eta);
        }
        }

 	for ( int eta = 1; eta <= etaSize; eta++)
	{
		cellEtaCorr[eta][n] = CellsEta[n]->GetBinContent(eta);
	}

        }
	
	std::cout<< " The reweighting coeffiecient is : "<<name<<std::endl;
	for(int j = 0; j<nEtaBins; j++) delete cellsDelta[j];
	for(int i = 0; i<nEtaBins; i++) delete CellsEta[i];
	delete Coeff;


	TFile* Coeff2 = new TFile("Correction.root","READ");
	for (int k = 0; k <clusterSize; k++) {
  	for (int n = 0; n < nEtaBins; n++) {
  	
		CellCorrection2[k][n] = (TProfile*) Coeff2->Get(Form("Correction_Cell_%i_%s_Eta_%1.2f_%1.2f",k+1,Type[1].c_str(),etaLimits[n], etaLimits[n+1]));
	}
	
	}
	*/
	
}
float Energy(int eta, int phi, std::vector<float> *clusterEnergy) 
{
    int etaSize = 7;
    int phiSize = 11;
    
    int etaMin = etaSize - (etaSize + eta)/2;
    int etaMax = etaSize - (etaSize - eta)/2;
    int phiMin = phiSize - (phiSize + phi)/2;
    int phiMax = phiSize - (phiSize - phi)/2;
    float sumEE = 0;
    
    for ( int e = etaMin; e < etaMax; e ++ ) {
        for ( int p = phiMin; p < phiMax; p ++ ) {
            sumEE += clusterEnergy->at(p+phiSize*e);
        }
    }
        return sumEE;
}
float calcWeta(std::vector<float> *clusterEnergy, std::vector<float> *clusterEta) 
{
   
    float sumE3x5 = 0;
    float sumEEta = 0;
    float sumEEtaSq = 0;
    
    for ( int phi = 3; phi < 8; phi ++ ) {
    for ( int eta = 2; eta < 5; eta ++ ) {
        
            sumE3x5 += clusterEnergy->at(phi+11*eta);
            sumEEta += clusterEnergy->at(phi+11*eta)*abs(clusterEta->at(phi+11*eta));
            sumEEtaSq += clusterEnergy->at(phi+11*eta)*clusterEta->at(phi+11*eta)*clusterEta->at(phi+11*eta);
        }
    }
    
    return sqrt ( sumEEtaSq/sumE3x5 - pow((sumEEta/sumE3x5),2) );
}

float getXsec(TChain* fChain)
{

	TFile* file = (TFile*)fChain->GetFile();
	TString name = file->GetName();
	float lumi = 43.8;
	float sigma = 0;
	float nevent = 0;
	if(name.Contains("301535") or name.Contains("301536"))
	{
		sigma = 52.7*1e3; nevent = 5*1e6;
	}

	if(name.Contains("301899") or name.Contains("301902"))
	{
		sigma = 5.24*1e3; nevent = 500*1e3;
	}
	if(name.Contains("301900") or name.Contains("301903"))
	{
		sigma = 0.38*1e3; nevent = 250*1e3;
	}
	if(name.Contains("data"))
	{
		return 1.;
	}
	return (sigma*lumi)/nevent;
}


void SetMatchedTreeBranches(TTree *fChain)
{


   fChain->Branch("MC_ph_Pt", &MC_ph_Pt);
   fChain->Branch("MC_ph_Eta", &MC_ph_Eta);
   fChain->Branch("MC_ph_Phi", &MC_ph_Phi);
   fChain->Branch("MC_ph_E", &MC_ph_E);
   fChain->Branch("MC_ph_topoetcone20", &MC_ph_topoetcone20);
   fChain->Branch("MC_ph_topoetcone30", &MC_ph_topoetcone30);
   fChain->Branch("MC_ph_topoetcone40", &MC_ph_topoetcone40);
   fChain->Branch("MC_ph_ptcone20", &MC_ph_ptcone20);
   fChain->Branch("MC_ph_ptcone30", &MC_ph_ptcone30);
   fChain->Branch("MC_ph_ptcone40", &MC_ph_ptcone40);
   fChain->Branch("MC_ph_etcone20", &MC_ph_etcone20);
   fChain->Branch("MC_ph_etcone30", &MC_ph_etcone30);
   fChain->Branch("MC_ph_etcone40", &MC_ph_etcone40);
   fChain->Branch("MC_ph_ptvarcone20", &MC_ph_ptvarcone20);
   fChain->Branch("MC_ph_ptvarcone30", &MC_ph_ptvarcone30);
   fChain->Branch("MC_ph_ptvarcone40", &MC_ph_ptvarcone40);
   fChain->Branch("MC_ph_isIsoLooseWP", &MC_ph_isIsoLooseWP);
   fChain->Branch("MC_ph_isIsoTightWP", &MC_ph_isIsoTightWP);
   fChain->Branch("MC_ph_isIsoTightCaloOnlyWP", &MC_ph_isIsoTightCaloOnlyWP);
   fChain->Branch("MC_ph_Conversion", &MC_ph_Conversion);
   fChain->Branch("MC_ph_Ethad", &MC_ph_Ethad);
   fChain->Branch("MC_ph_Ethad1", &MC_ph_Ethad1);
   fChain->Branch("MC_ph_Rhad1", &MC_ph_Rhad1);
   fChain->Branch("MC_ph_Rhad", &MC_ph_Rhad);
   fChain->Branch("MC_ph_E011", &MC_ph_E011);
   fChain->Branch("MC_ph_E132", &MC_ph_E132);
   fChain->Branch("MC_ph_E237", &MC_ph_E237);
   fChain->Branch("MC_ph_E277", &MC_ph_E277);
   fChain->Branch("MC_ph_Reta", &MC_ph_Reta);
   fChain->Branch("MC_ph_Rphi", &MC_ph_Rphi);
   fChain->Branch("MC_ph_Weta1", &MC_ph_Weta1);
   fChain->Branch("MC_ph_Weta2", &MC_ph_Weta2);
   fChain->Branch("MC_ph_f1", &MC_ph_f1);
   fChain->Branch("MC_ph_f3", &MC_ph_f3);
   fChain->Branch("MC_ph_f3core", &MC_ph_f3core);
   fChain->Branch("MC_ph_fracs1", &MC_ph_fracs1);
   fChain->Branch("MC_ph_Wstot1", &MC_ph_Wstot1);
   fChain->Branch("MC_ph_deltaE", &MC_ph_deltaE);
   fChain->Branch("MC_ph_Eratio", &MC_ph_Eratio);
   fChain->Branch("MC_ph_E2tsts1", &MC_ph_E2tsts1);
   fChain->Branch("MC_ph_Emins1", &MC_ph_Emins1);
   fChain->Branch("MC_ph_Emaxs1", &MC_ph_Emaxs1);
   fChain->Branch("MC_ph_ClusterSize7x11Lr2", &MC_ph_ClusterSize7x11Lr2);
   fChain->Branch("MC_ph_ClusterSize3x7Lr2", &MC_ph_ClusterSize3x7Lr2);
   fChain->Branch("MC_ph_ClusterSize5x5Lr2", &MC_ph_ClusterSize5x5Lr2);
   fChain->Branch("MC_ph_ClusterSize7x11Lr3", &MC_ph_ClusterSize7x11Lr3);
   fChain->Branch("MC_ph_ClusterSize3x7Lr3", &MC_ph_ClusterSize3x7Lr3);
   fChain->Branch("MC_ph_ClusterSize5x5Lr3", &MC_ph_ClusterSize5x5Lr3);
   fChain->Branch("MC_ph_ClusterSize7x11Lr1", &MC_ph_ClusterSize7x11Lr1);
   fChain->Branch("MC_ph_ClusterSize3x7Lr1", &MC_ph_ClusterSize3x7Lr1);
   fChain->Branch("MC_ph_ClusterSize5x5Lr1", &MC_ph_ClusterSize5x5Lr1);
   fChain->Branch("MC_ph_clusterCellsLr2E7x11", &MC_ph_clusterCellsLr2E7x11);
   fChain->Branch("MC_ph_clusterCellsLr2Eta7x11", &MC_ph_clusterCellsLr2Eta7x11);
   fChain->Branch("MC_ph_clusterCellsLr2Phi7x11", &MC_ph_clusterCellsLr2Phi7x11);
   fChain->Branch("MC_ph_clusterCellsLr2E3x7", &MC_ph_clusterCellsLr2E3x7);
   fChain->Branch("MC_ph_clusterCellsLr2Eta3x7", &MC_ph_clusterCellsLr2Eta3x7);
   fChain->Branch("MC_ph_clusterCellsLr2Phi3x7", &MC_ph_clusterCellsLr2Phi3x7);
   fChain->Branch("MC_ph_clusterCellsLr2E5x5", &MC_ph_clusterCellsLr2E5x5);
   fChain->Branch("MC_ph_clusterCellsLr2Eta5x5", &MC_ph_clusterCellsLr2Eta5x5);
   fChain->Branch("MC_ph_clusterCellsLr2Phi5x5", &MC_ph_clusterCellsLr2Phi5x5);
   fChain->Branch("MC_ph_clusterCellsLr1E7x11", &MC_ph_clusterCellsLr1E7x11);
   fChain->Branch("MC_ph_clusterCellsLr1Eta7x11", &MC_ph_clusterCellsLr1Eta7x11);
   fChain->Branch("MC_ph_clusterCellsLr1Phi7x11", &MC_ph_clusterCellsLr1Phi7x11);
   fChain->Branch("MC_ph_clusterCellsLr1E3x7", &MC_ph_clusterCellsLr1E3x7);
   fChain->Branch("MC_ph_clusterCellsLr1Eta3x7", &MC_ph_clusterCellsLr1Eta3x7);
   fChain->Branch("MC_ph_clusterCellsLr1Phi3x7", &MC_ph_clusterCellsLr1Phi3x7);
   fChain->Branch("MC_ph_clusterCellsLr1E5x5", &MC_ph_clusterCellsLr1E5x5);
   fChain->Branch("MC_ph_clusterCellsLr1Eta5x5", &MC_ph_clusterCellsLr1Eta5x5);
   fChain->Branch("MC_ph_clusterCellsLr1Phi5x5", &MC_ph_clusterCellsLr1Phi5x5);
   fChain->Branch("MC_ph_clusterCellsLr3E7x11", &MC_ph_clusterCellsLr3E7x11);
   fChain->Branch("MC_ph_clusterCellsLr3Eta7x11", &MC_ph_clusterCellsLr3Eta7x11);
   fChain->Branch("MC_ph_clusterCellsLr3Phi7x11", &MC_ph_clusterCellsLr3Phi7x11);
   fChain->Branch("MC_ph_clusterCellsLr3E3x7", &MC_ph_clusterCellsLr3E3x7);
   fChain->Branch("MC_ph_clusterCellsLr3Eta3x7", &MC_ph_clusterCellsLr3Eta3x7);
   fChain->Branch("MC_ph_clusterCellsLr3Phi3x7", &MC_ph_clusterCellsLr3Phi3x7);
   fChain->Branch("MC_ph_clusterCellsLr3E5x5", &MC_ph_clusterCellsLr3E5x5);
   fChain->Branch("MC_ph_clusterCellsLr3Eta5x5", &MC_ph_clusterCellsLr3Eta5x5);
   fChain->Branch("MC_ph_clusterCellsLr3Phi5x5", &MC_ph_clusterCellsLr3Phi5x5);
   fChain->Branch("MC_l1_Pt", &MC_l1_Pt);
   fChain->Branch("MC_l1_Eta", &MC_l1_Eta);
   fChain->Branch("MC_l1_Phi", &MC_l1_Phi);
   fChain->Branch("MC_l1_E", &MC_l1_E);
   fChain->Branch("MC_l1_Charge", &MC_l1_Charge);
   fChain->Branch("MC_l2_Pt", &MC_l2_Pt);
   fChain->Branch("MC_l2_Eta", &MC_l2_Eta);
   fChain->Branch("MC_l2_Phi", &MC_l2_Phi);
   fChain->Branch("MC_l2_E", &MC_l2_E);
   fChain->Branch("MC_l2_Charge", &MC_l2_Charge);
   fChain->Branch("MC_l1_SF", &MC_l1_SF);
   fChain->Branch("MC_l2_SF", &MC_l2_SF);
   fChain->Branch("MC_pu_wgt", &MC_pu_wgt);
   fChain->Branch("MC_Mu", &MC_Mu);
   fChain->Branch("MC_mc_wgt", &MC_mc_wgt);
   fChain->Branch("MC_mc_xsec", &MC_mc_xsec);
   fChain->Branch("MC_isZeey", &MC_isZeey);
   fChain->Branch("MC_isZmumuy", &MC_isZmumuy);
   fChain->Branch("MC_M_ee", &MC_M_ee);
   fChain->Branch("MC_M_eey", &MC_M_eey);
   fChain->Branch("MC_M_mumu", &MC_M_mumu);
   fChain->Branch("MC_M_mumuy", &MC_M_mumuy);

   fChain->Branch("MC_i", &MC_i);
   fChain->Branch("Data_i", &Data_i);


   fChain->Branch("Data_ph_Pt", &Data_ph_Pt);
   fChain->Branch("Data_ph_Eta", &Data_ph_Eta);
   fChain->Branch("Data_ph_Phi", &Data_ph_Phi);
   fChain->Branch("Data_ph_E", &Data_ph_E);
   fChain->Branch("Data_ph_topoetcone20", &Data_ph_topoetcone20);
   fChain->Branch("Data_ph_topoetcone30", &Data_ph_topoetcone30);
   fChain->Branch("Data_ph_topoetcone40", &Data_ph_topoetcone40);
   fChain->Branch("Data_ph_ptcone20", &Data_ph_ptcone20);
   fChain->Branch("Data_ph_ptcone30", &Data_ph_ptcone30);
   fChain->Branch("Data_ph_ptcone40", &Data_ph_ptcone40);
   fChain->Branch("Data_ph_etcone20", &Data_ph_etcone20);
   fChain->Branch("Data_ph_etcone30", &Data_ph_etcone30);
   fChain->Branch("Data_ph_etcone40", &Data_ph_etcone40);
   fChain->Branch("Data_ph_ptvarcone20", &Data_ph_ptvarcone20);
   fChain->Branch("Data_ph_ptvarcone30", &Data_ph_ptvarcone30);
   fChain->Branch("Data_ph_ptvarcone40", &Data_ph_ptvarcone40);
   fChain->Branch("Data_ph_isIsoLooseWP", &Data_ph_isIsoLooseWP);
   fChain->Branch("Data_ph_isIsoTightWP", &Data_ph_isIsoTightWP);
   fChain->Branch("Data_ph_isIsoTightCaloOnlyWP", &Data_ph_isIsoTightCaloOnlyWP);
   fChain->Branch("Data_ph_Conversion", &Data_ph_Conversion);
   fChain->Branch("Data_ph_Ethad", &Data_ph_Ethad);
   fChain->Branch("Data_ph_Ethad1", &Data_ph_Ethad1);
   fChain->Branch("Data_ph_Rhad1", &Data_ph_Rhad1);
   fChain->Branch("Data_ph_Rhad", &Data_ph_Rhad);
   fChain->Branch("Data_ph_E011", &Data_ph_E011);
   fChain->Branch("Data_ph_E132", &Data_ph_E132);
   fChain->Branch("Data_ph_E237", &Data_ph_E237);
   fChain->Branch("Data_ph_E277", &Data_ph_E277);
   fChain->Branch("Data_ph_Reta", &Data_ph_Reta);
   fChain->Branch("Data_ph_Rphi", &Data_ph_Rphi);
   fChain->Branch("Data_ph_Weta1", &Data_ph_Weta1);
   fChain->Branch("Data_ph_Weta2", &Data_ph_Weta2);
   fChain->Branch("Data_ph_f1", &Data_ph_f1);
   fChain->Branch("Data_ph_f3", &Data_ph_f3);
   fChain->Branch("Data_ph_f3core", &Data_ph_f3core);
   fChain->Branch("Data_ph_fracs1", &Data_ph_fracs1);
   fChain->Branch("Data_ph_Wstot1", &Data_ph_Wstot1);
   fChain->Branch("Data_ph_deltaE", &Data_ph_deltaE);
   fChain->Branch("Data_ph_Eratio", &Data_ph_Eratio);
   fChain->Branch("Data_ph_E2tsts1", &Data_ph_E2tsts1);
   fChain->Branch("Data_ph_Emins1", &Data_ph_Emins1);
   fChain->Branch("Data_ph_Emaxs1", &Data_ph_Emaxs1);
   fChain->Branch("Data_ph_ClusterSize7x11Lr2", &Data_ph_ClusterSize7x11Lr2);
   fChain->Branch("Data_ph_ClusterSize3x7Lr2", &Data_ph_ClusterSize3x7Lr2);
   fChain->Branch("Data_ph_ClusterSize5x5Lr2", &Data_ph_ClusterSize5x5Lr2);
   fChain->Branch("Data_ph_ClusterSize7x11Lr3", &Data_ph_ClusterSize7x11Lr3);
   fChain->Branch("Data_ph_ClusterSize3x7Lr3", &Data_ph_ClusterSize3x7Lr3);
   fChain->Branch("Data_ph_ClusterSize5x5Lr3", &Data_ph_ClusterSize5x5Lr3);
   fChain->Branch("Data_ph_ClusterSize7x11Lr1", &Data_ph_ClusterSize7x11Lr1);
   fChain->Branch("Data_ph_ClusterSize3x7Lr1", &Data_ph_ClusterSize3x7Lr1);
   fChain->Branch("Data_ph_ClusterSize5x5Lr1", &Data_ph_ClusterSize5x5Lr1);
   fChain->Branch("Data_ph_clusterCellsLr2E7x11", &Data_ph_clusterCellsLr2E7x11);
   fChain->Branch("Data_ph_clusterCellsLr2Eta7x11", &Data_ph_clusterCellsLr2Eta7x11);
   fChain->Branch("Data_ph_clusterCellsLr2Phi7x11", &Data_ph_clusterCellsLr2Phi7x11);
   fChain->Branch("Data_ph_clusterCellsLr2E3x7", &Data_ph_clusterCellsLr2E3x7);
   fChain->Branch("Data_ph_clusterCellsLr2Eta3x7", &Data_ph_clusterCellsLr2Eta3x7);
   fChain->Branch("Data_ph_clusterCellsLr2Phi3x7", &Data_ph_clusterCellsLr2Phi3x7);
   fChain->Branch("Data_ph_clusterCellsLr2E5x5", &Data_ph_clusterCellsLr2E5x5);
   fChain->Branch("Data_ph_clusterCellsLr2Eta5x5", &Data_ph_clusterCellsLr2Eta5x5);
   fChain->Branch("Data_ph_clusterCellsLr2Phi5x5", &Data_ph_clusterCellsLr2Phi5x5);
   fChain->Branch("Data_ph_clusterCellsLr1E7x11", &Data_ph_clusterCellsLr1E7x11);
   fChain->Branch("Data_ph_clusterCellsLr1Eta7x11", &Data_ph_clusterCellsLr1Eta7x11);
   fChain->Branch("Data_ph_clusterCellsLr1Phi7x11", &Data_ph_clusterCellsLr1Phi7x11);
   fChain->Branch("Data_ph_clusterCellsLr1E3x7", &Data_ph_clusterCellsLr1E3x7);
   fChain->Branch("Data_ph_clusterCellsLr1Eta3x7", &Data_ph_clusterCellsLr1Eta3x7);
   fChain->Branch("Data_ph_clusterCellsLr1Phi3x7", &Data_ph_clusterCellsLr1Phi3x7);
   fChain->Branch("Data_ph_clusterCellsLr1E5x5", &Data_ph_clusterCellsLr1E5x5);
   fChain->Branch("Data_ph_clusterCellsLr1Eta5x5", &Data_ph_clusterCellsLr1Eta5x5);
   fChain->Branch("Data_ph_clusterCellsLr1Phi5x5", &Data_ph_clusterCellsLr1Phi5x5);
   fChain->Branch("Data_ph_clusterCellsLr3E7x11", &Data_ph_clusterCellsLr3E7x11);
   fChain->Branch("Data_ph_clusterCellsLr3Eta7x11", &Data_ph_clusterCellsLr3Eta7x11);
   fChain->Branch("Data_ph_clusterCellsLr3Phi7x11", &Data_ph_clusterCellsLr3Phi7x11);
   fChain->Branch("Data_ph_clusterCellsLr3E3x7", &Data_ph_clusterCellsLr3E3x7);
   fChain->Branch("Data_ph_clusterCellsLr3Eta3x7", &Data_ph_clusterCellsLr3Eta3x7);
   fChain->Branch("Data_ph_clusterCellsLr3Phi3x7", &Data_ph_clusterCellsLr3Phi3x7);
   fChain->Branch("Data_ph_clusterCellsLr3E5x5", &Data_ph_clusterCellsLr3E5x5);
   fChain->Branch("Data_ph_clusterCellsLr3Eta5x5", &Data_ph_clusterCellsLr3Eta5x5);
   fChain->Branch("Data_ph_clusterCellsLr3Phi5x5", &Data_ph_clusterCellsLr3Phi5x5);
   fChain->Branch("Data_l1_Pt", &Data_l1_Pt);
   fChain->Branch("Data_l1_Eta", &Data_l1_Eta);
   fChain->Branch("Data_l1_Phi", &Data_l1_Phi);
   fChain->Branch("Data_l1_E", &Data_l1_E);
   fChain->Branch("Data_l1_Charge", &Data_l1_Charge);
   fChain->Branch("Data_l2_Pt", &Data_l2_Pt);
   fChain->Branch("Data_l2_Eta", &Data_l2_Eta);
   fChain->Branch("Data_l2_Phi", &Data_l2_Phi);
   fChain->Branch("Data_l2_E", &Data_l2_E);
   fChain->Branch("Data_l2_Charge", &Data_l2_Charge);
   fChain->Branch("Data_l1_SF", &Data_l1_SF);
   fChain->Branch("Data_l2_SF", &Data_l2_SF);
   fChain->Branch("Data_pu_wgt", &Data_pu_wgt);
   fChain->Branch("Data_Mu", &Data_Mu);
   fChain->Branch("Data_mc_wgt", &Data_mc_wgt);
   fChain->Branch("Data_mc_xsec", &Data_mc_xsec);
   fChain->Branch("Data_isZeey", &Data_isZeey);
   fChain->Branch("Data_isZmumuy", &Data_isZmumuy);
   fChain->Branch("Data_M_ee", &Data_M_ee);
   fChain->Branch("Data_M_eey", &Data_M_eey);
   fChain->Branch("Data_M_mumu", &Data_M_mumu);
   fChain->Branch("Data_M_mumuy", &Data_M_mumuy);
}

