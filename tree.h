//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Sep 26 10:26:40 2019 by ROOT version 6.18/00
// from TTree tree/matched Data_MC
// found on file: Tree_matched.root
//////////////////////////////////////////////////////////

#ifndef tree_h
#define tree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class tree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

    const int etaSize = 7;
    const int phiSize = 11;
    const int clusterSize = 77;
  
    std::string Type[3] = {"Inc","NCon","Con"};
    std::string _target[3] = {"Zlly","Zeey","Zmumuy"}; //,"Zmumuy","Inc"};
    int const nEtaBins = 14;
    const float etaLimits [14 +1] = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.3, 1.37, 1.52, 1.6, 1.80, 2.0, 2.2, 2.4};
    int const nEBins = 12;
    const float EBins [12 + 1] = {-1., -0.5, -0.4, -0.3, -0.2, -0.1, 0. , 0.1, 0.2, 0.3, 0.4, 0.5, 1.};

    int const nEBins2 = 13;
    const float EBins2 [13 + 1] = {-1.,-0.5,0.,0.5,1.,1.5,2.,2.5,3.,4.,5.,6.,10.,15.};

    bool doRew = false;
    bool compute_corr = true;

    TProfile* CellCorrection[77][14][3];
    TProfile* ClusterCorrection[14][3];

    TH1F* Reta[3][14];
    TH1F* Rphi[3][14];
 
    TH1F* RetaData[3][14];
    TH1F* RphiData[3][14];

    TH1F* RetaRew[3][14];
    TH1F* RphiRew[3][14];

    TH1F* RphiRatio[2][3][14];
    TH1F* RetaRatio[2][3][14];


   // Declaration of leaf types
   Double_t        MC_ph_Pt;
   Double_t        MC_ph_Eta;
   Double_t        MC_ph_Phi;
   Double_t        MC_ph_E;
   Double_t        MC_ph_topoetcone20;
   Double_t        MC_ph_topoetcone30;
   Double_t        MC_ph_topoetcone40;
   Double_t        MC_ph_ptcone20;
   Double_t        MC_ph_ptcone30;
   Double_t        MC_ph_ptcone40;
   Double_t        MC_ph_etcone20;
   Double_t        MC_ph_etcone30;
   Double_t        MC_ph_etcone40;
   Double_t        MC_ph_ptvarcone20;
   Double_t        MC_ph_ptvarcone30;
   Double_t        MC_ph_ptvarcone40;
   Bool_t          MC_ph_isIsoLooseWP;
   Bool_t          MC_ph_isIsoTightWP;
   Bool_t          MC_ph_isIsoTightCaloOnlyWP;
   Double_t        MC_ph_Conversion;
   Double_t        MC_ph_Ethad;
   Double_t        MC_ph_Ethad1;
   Double_t        MC_ph_Rhad1;
   Double_t        MC_ph_Rhad;
   Double_t        MC_ph_E011;
   Double_t        MC_ph_E132;
   Double_t        MC_ph_E237;
   Double_t        MC_ph_E277;
   Double_t        MC_ph_Reta;
   Double_t        MC_ph_Rphi;
   Double_t        MC_ph_Weta1;
   Double_t        MC_ph_Weta2;
   Double_t        MC_ph_f1;
   Double_t        MC_ph_f3;
   Double_t        MC_ph_f3core;
   Double_t        MC_ph_fracs1;
   Double_t        MC_ph_Wstot1;
   Double_t        MC_ph_deltaE;
   Double_t        MC_ph_Eratio;
   Double_t        MC_ph_E2tsts1;
   Double_t        MC_ph_Emins1;
   Double_t        MC_ph_Emaxs1;
   Int_t           MC_ph_ClusterSize7x11Lr2;
   Int_t           MC_ph_ClusterSize3x7Lr2;
   Int_t           MC_ph_ClusterSize5x5Lr2;
   Int_t           MC_ph_ClusterSize7x11Lr3;
   Int_t           MC_ph_ClusterSize3x7Lr3;
   Int_t           MC_ph_ClusterSize5x5Lr3;
   Int_t           MC_ph_ClusterSize7x11Lr1;
   Int_t           MC_ph_ClusterSize3x7Lr1;
   Int_t           MC_ph_ClusterSize5x5Lr1;
   vector<float>   *MC_ph_clusterCellsLr2E7x11;
   vector<float>   *MC_ph_clusterCellsLr2Eta7x11;
   vector<float>   *MC_ph_clusterCellsLr2Phi7x11;
   vector<float>   *MC_ph_clusterCellsLr2E3x7;
   vector<float>   *MC_ph_clusterCellsLr2Eta3x7;
   vector<float>   *MC_ph_clusterCellsLr2Phi3x7;
   vector<float>   *MC_ph_clusterCellsLr2E5x5;
   vector<float>   *MC_ph_clusterCellsLr2Eta5x5;
   vector<float>   *MC_ph_clusterCellsLr2Phi5x5;
   vector<float>   *MC_ph_clusterCellsLr1E7x11;
   vector<float>   *MC_ph_clusterCellsLr1Eta7x11;
   vector<float>   *MC_ph_clusterCellsLr1Phi7x11;
   vector<float>   *MC_ph_clusterCellsLr1E3x7;
   vector<float>   *MC_ph_clusterCellsLr1Eta3x7;
   vector<float>   *MC_ph_clusterCellsLr1Phi3x7;
   vector<float>   *MC_ph_clusterCellsLr1E5x5;
   vector<float>   *MC_ph_clusterCellsLr1Eta5x5;
   vector<float>   *MC_ph_clusterCellsLr1Phi5x5;
   vector<float>   *MC_ph_clusterCellsLr3E7x11;
   vector<float>   *MC_ph_clusterCellsLr3Eta7x11;
   vector<float>   *MC_ph_clusterCellsLr3Phi7x11;
   vector<float>   *MC_ph_clusterCellsLr3E3x7;
   vector<float>   *MC_ph_clusterCellsLr3Eta3x7;
   vector<float>   *MC_ph_clusterCellsLr3Phi3x7;
   vector<float>   *MC_ph_clusterCellsLr3E5x5;
   vector<float>   *MC_ph_clusterCellsLr3Eta5x5;
   vector<float>   *MC_ph_clusterCellsLr3Phi5x5;
   Double_t        MC_l1_Pt;
   Double_t        MC_l1_Eta;
   Double_t        MC_l1_Phi;
   Double_t        MC_l1_E;
   Double_t        MC_l1_Charge;
   Double_t        MC_l2_Pt;
   Double_t        MC_l2_Eta;
   Double_t        MC_l2_Phi;
   Double_t        MC_l2_E;
   Double_t        MC_l2_Charge;
   Double_t        MC_l1_SF;
   Double_t        MC_l2_SF;
   Double_t        MC_pu_wgt;
   Double_t        MC_Mu;
   Double_t        MC_mc_wgt;
   Double_t        MC_mc_xsec;
   Bool_t          MC_isZeey;
   Bool_t          MC_isZmumuy;
   vector<float>   *MC_M_ee;
   vector<float>   *MC_M_eey;
   vector<float>   *MC_M_mumu;
   vector<float>   *MC_M_mumuy;
   Int_t           MC_i;
   Int_t           Data_i;
   Double_t        Data_ph_Pt;
   Double_t        Data_ph_Eta;
   Double_t        Data_ph_Phi;
   Double_t        Data_ph_E;
   Double_t        Data_ph_topoetcone20;
   Double_t        Data_ph_topoetcone30;
   Double_t        Data_ph_topoetcone40;
   Double_t        Data_ph_ptcone20;
   Double_t        Data_ph_ptcone30;
   Double_t        Data_ph_ptcone40;
   Double_t        Data_ph_etcone20;
   Double_t        Data_ph_etcone30;
   Double_t        Data_ph_etcone40;
   Double_t        Data_ph_ptvarcone20;
   Double_t        Data_ph_ptvarcone30;
   Double_t        Data_ph_ptvarcone40;
   Bool_t          Data_ph_isIsoLooseWP;
   Bool_t          Data_ph_isIsoTightWP;
   Bool_t          Data_ph_isIsoTightCaloOnlyWP;
   Double_t        Data_ph_Conversion;
   Double_t        Data_ph_Ethad;
   Double_t        Data_ph_Ethad1;
   Double_t        Data_ph_Rhad1;
   Double_t        Data_ph_Rhad;
   Double_t        Data_ph_E011;
   Double_t        Data_ph_E132;
   Double_t        Data_ph_E237;
   Double_t        Data_ph_E277;
   Double_t        Data_ph_Reta;
   Double_t        Data_ph_Rphi;
   Double_t        Data_ph_Weta1;
   Double_t        Data_ph_Weta2;
   Double_t        Data_ph_f1;
   Double_t        Data_ph_f3;
   Double_t        Data_ph_f3core;
   Double_t        Data_ph_fracs1;
   Double_t        Data_ph_Wstot1;
   Double_t        Data_ph_deltaE;
   Double_t        Data_ph_Eratio;
   Double_t        Data_ph_E2tsts1;
   Double_t        Data_ph_Emins1;
   Double_t        Data_ph_Emaxs1;
   Int_t           Data_ph_ClusterSize7x11Lr2;
   Int_t           Data_ph_ClusterSize3x7Lr2;
   Int_t           Data_ph_ClusterSize5x5Lr2;
   Int_t           Data_ph_ClusterSize7x11Lr3;
   Int_t           Data_ph_ClusterSize3x7Lr3;
   Int_t           Data_ph_ClusterSize5x5Lr3;
   Int_t           Data_ph_ClusterSize7x11Lr1;
   Int_t           Data_ph_ClusterSize3x7Lr1;
   Int_t           Data_ph_ClusterSize5x5Lr1;
   vector<float>   *Data_ph_clusterCellsLr2E7x11;
   vector<float>   *Data_ph_clusterCellsLr2Eta7x11;
   vector<float>   *Data_ph_clusterCellsLr2Phi7x11;
   vector<float>   *Data_ph_clusterCellsLr2E3x7;
   vector<float>   *Data_ph_clusterCellsLr2Eta3x7;
   vector<float>   *Data_ph_clusterCellsLr2Phi3x7;
   vector<float>   *Data_ph_clusterCellsLr2E5x5;
   vector<float>   *Data_ph_clusterCellsLr2Eta5x5;
   vector<float>   *Data_ph_clusterCellsLr2Phi5x5;
   vector<float>   *Data_ph_clusterCellsLr1E7x11;
   vector<float>   *Data_ph_clusterCellsLr1Eta7x11;
   vector<float>   *Data_ph_clusterCellsLr1Phi7x11;
   vector<float>   *Data_ph_clusterCellsLr1E3x7;
   vector<float>   *Data_ph_clusterCellsLr1Eta3x7;
   vector<float>   *Data_ph_clusterCellsLr1Phi3x7;
   vector<float>   *Data_ph_clusterCellsLr1E5x5;
   vector<float>   *Data_ph_clusterCellsLr1Eta5x5;
   vector<float>   *Data_ph_clusterCellsLr1Phi5x5;
   vector<float>   *Data_ph_clusterCellsLr3E7x11;
   vector<float>   *Data_ph_clusterCellsLr3Eta7x11;
   vector<float>   *Data_ph_clusterCellsLr3Phi7x11;
   vector<float>   *Data_ph_clusterCellsLr3E3x7;
   vector<float>   *Data_ph_clusterCellsLr3Eta3x7;
   vector<float>   *Data_ph_clusterCellsLr3Phi3x7;
   vector<float>   *Data_ph_clusterCellsLr3E5x5;
   vector<float>   *Data_ph_clusterCellsLr3Eta5x5;
   vector<float>   *Data_ph_clusterCellsLr3Phi5x5;
   Double_t        Data_l1_Pt;
   Double_t        Data_l1_Eta;
   Double_t        Data_l1_Phi;
   Double_t        Data_l1_E;
   Double_t        Data_l1_Charge;
   Double_t        Data_l2_Pt;
   Double_t        Data_l2_Eta;
   Double_t        Data_l2_Phi;
   Double_t        Data_l2_E;
   Double_t        Data_l2_Charge;
   Double_t        Data_l1_SF;
   Double_t        Data_l2_SF;
   Double_t        Data_pu_wgt;
   Double_t        Data_Mu;
   Double_t        Data_mc_wgt;
   Double_t        Data_mc_xsec;
   Bool_t          Data_isZeey;
   Bool_t          Data_isZmumuy;
   vector<float>   *Data_M_ee;
   vector<float>   *Data_M_eey;
   vector<float>   *Data_M_mumu;
   vector<float>   *Data_M_mumuy;

   // List of branches
   TBranch        *b_MC_ph_Pt;   //!
   TBranch        *b_MC_ph_Eta;   //!
   TBranch        *b_MC_ph_Phi;   //!
   TBranch        *b_MC_ph_E;   //!
   TBranch        *b_MC_ph_topoetcone20;   //!
   TBranch        *b_MC_ph_topoetcone30;   //!
   TBranch        *b_MC_ph_topoetcone40;   //!
   TBranch        *b_MC_ph_ptcone20;   //!
   TBranch        *b_MC_ph_ptcone30;   //!
   TBranch        *b_MC_ph_ptcone40;   //!
   TBranch        *b_MC_ph_etcone20;   //!
   TBranch        *b_MC_ph_etcone30;   //!
   TBranch        *b_MC_ph_etcone40;   //!
   TBranch        *b_MC_ph_ptvarcone20;   //!
   TBranch        *b_MC_ph_ptvarcone30;   //!
   TBranch        *b_MC_ph_ptvarcone40;   //!
   TBranch        *b_MC_ph_isIsoLooseWP;   //!
   TBranch        *b_MC_ph_isIsoTightWP;   //!
   TBranch        *b_MC_ph_isIsoTightCaloOnlyWP;   //!
   TBranch        *b_MC_ph_Conversion;   //!
   TBranch        *b_MC_ph_Ethad;   //!
   TBranch        *b_MC_ph_Ethad1;   //!
   TBranch        *b_MC_ph_Rhad1;   //!
   TBranch        *b_MC_ph_Rhad;   //!
   TBranch        *b_MC_ph_E011;   //!
   TBranch        *b_MC_ph_E132;   //!
   TBranch        *b_MC_ph_E237;   //!
   TBranch        *b_MC_ph_E277;   //!
   TBranch        *b_MC_ph_Reta;   //!
   TBranch        *b_MC_ph_Rphi;   //!
   TBranch        *b_MC_ph_Weta1;   //!
   TBranch        *b_MC_ph_Weta2;   //!
   TBranch        *b_MC_ph_f1;   //!
   TBranch        *b_MC_ph_f3;   //!
   TBranch        *b_MC_ph_f3core;   //!
   TBranch        *b_MC_ph_fracs1;   //!
   TBranch        *b_MC_ph_Wstot1;   //!
   TBranch        *b_MC_ph_deltaE;   //!
   TBranch        *b_MC_ph_Eratio;   //!
   TBranch        *b_MC_ph_E2tsts1;   //!
   TBranch        *b_MC_ph_Emins1;   //!
   TBranch        *b_MC_ph_Emaxs1;   //!
   TBranch        *b_MC_ph_ClusterSize7x11Lr2;   //!
   TBranch        *b_MC_ph_ClusterSize3x7Lr2;   //!
   TBranch        *b_MC_ph_ClusterSize5x5Lr2;   //!
   TBranch        *b_MC_ph_ClusterSize7x11Lr3;   //!
   TBranch        *b_MC_ph_ClusterSize3x7Lr3;   //!
   TBranch        *b_MC_ph_ClusterSize5x5Lr3;   //!
   TBranch        *b_MC_ph_ClusterSize7x11Lr1;   //!
   TBranch        *b_MC_ph_ClusterSize3x7Lr1;   //!
   TBranch        *b_MC_ph_ClusterSize5x5Lr1;   //!
   TBranch        *b_MC_ph_clusterCellsLr2E7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr2Eta7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr2Phi7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr2E3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr2Eta3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr2Phi3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr2E5x5;   //!
   TBranch        *b_MC_ph_clusterCellsLr2Eta5x5;   //!
   TBranch        *b_MC_ph_clusterCellsLr2Phi5x5;   //!
   TBranch        *b_MC_ph_clusterCellsLr1E7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr1Eta7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr1Phi7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr1E3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr1Eta3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr1Phi3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr1E5x5;   //!
   TBranch        *b_MC_ph_clusterCellsLr1Eta5x5;   //!
   TBranch        *b_MC_ph_clusterCellsLr1Phi5x5;   //!
   TBranch        *b_MC_ph_clusterCellsLr3E7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr3Eta7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr3Phi7x11;   //!
   TBranch        *b_MC_ph_clusterCellsLr3E3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr3Eta3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr3Phi3x7;   //!
   TBranch        *b_MC_ph_clusterCellsLr3E5x5;   //!
   TBranch        *b_MC_ph_clusterCellsLr3Eta5x5;   //!
   TBranch        *b_MC_ph_clusterCellsLr3Phi5x5;   //!
   TBranch        *b_MC_l1_Pt;   //!
   TBranch        *b_MC_l1_Eta;   //!
   TBranch        *b_MC_l1_Phi;   //!
   TBranch        *b_MC_l1_E;   //!
   TBranch        *b_MC_l1_Charge;   //!
   TBranch        *b_MC_l2_Pt;   //!
   TBranch        *b_MC_l2_Eta;   //!
   TBranch        *b_MC_l2_Phi;   //!
   TBranch        *b_MC_l2_E;   //!
   TBranch        *b_MC_l2_Charge;   //!
   TBranch        *b_MC_l1_SF;   //!
   TBranch        *b_MC_l2_SF;   //!
   TBranch        *b_MC_pu_wgt;   //!
   TBranch        *b_MC_Mu;   //!
   TBranch        *b_MC_mc_wgt;   //!
   TBranch        *b_MC_mc_xsec;   //!
   TBranch        *b_MC_isZeey;   //!
   TBranch        *b_MC_isZmumuy;   //!
   TBranch        *b_MC_M_ee;   //!
   TBranch        *b_MC_M_eey;   //!
   TBranch        *b_MC_M_mumu;   //!
   TBranch        *b_MC_M_mumuy;   //!
   TBranch        *b_MC_i;   //!
   TBranch        *b_Data_i;   //!
   TBranch        *b_Data_ph_Pt;   //!
   TBranch        *b_Data_ph_Eta;   //!
   TBranch        *b_Data_ph_Phi;   //!
   TBranch        *b_Data_ph_E;   //!
   TBranch        *b_Data_ph_topoetcone20;   //!
   TBranch        *b_Data_ph_topoetcone30;   //!
   TBranch        *b_Data_ph_topoetcone40;   //!
   TBranch        *b_Data_ph_ptcone20;   //!
   TBranch        *b_Data_ph_ptcone30;   //!
   TBranch        *b_Data_ph_ptcone40;   //!
   TBranch        *b_Data_ph_etcone20;   //!
   TBranch        *b_Data_ph_etcone30;   //!
   TBranch        *b_Data_ph_etcone40;   //!
   TBranch        *b_Data_ph_ptvarcone20;   //!
   TBranch        *b_Data_ph_ptvarcone30;   //!
   TBranch        *b_Data_ph_ptvarcone40;   //!
   TBranch        *b_Data_ph_isIsoLooseWP;   //!
   TBranch        *b_Data_ph_isIsoTightWP;   //!
   TBranch        *b_Data_ph_isIsoTightCaloOnlyWP;   //!
   TBranch        *b_Data_ph_Conversion;   //!
   TBranch        *b_Data_ph_Ethad;   //!
   TBranch        *b_Data_ph_Ethad1;   //!
   TBranch        *b_Data_ph_Rhad1;   //!
   TBranch        *b_Data_ph_Rhad;   //!
   TBranch        *b_Data_ph_E011;   //!
   TBranch        *b_Data_ph_E132;   //!
   TBranch        *b_Data_ph_E237;   //!
   TBranch        *b_Data_ph_E277;   //!
   TBranch        *b_Data_ph_Reta;   //!
   TBranch        *b_Data_ph_Rphi;   //!
   TBranch        *b_Data_ph_Weta1;   //!
   TBranch        *b_Data_ph_Weta2;   //!
   TBranch        *b_Data_ph_f1;   //!
   TBranch        *b_Data_ph_f3;   //!
   TBranch        *b_Data_ph_f3core;   //!
   TBranch        *b_Data_ph_fracs1;   //!
   TBranch        *b_Data_ph_Wstot1;   //!
   TBranch        *b_Data_ph_deltaE;   //!
   TBranch        *b_Data_ph_Eratio;   //!
   TBranch        *b_Data_ph_E2tsts1;   //!
   TBranch        *b_Data_ph_Emins1;   //!
   TBranch        *b_Data_ph_Emaxs1;   //!
   TBranch        *b_Data_ph_ClusterSize7x11Lr2;   //!
   TBranch        *b_Data_ph_ClusterSize3x7Lr2;   //!
   TBranch        *b_Data_ph_ClusterSize5x5Lr2;   //!
   TBranch        *b_Data_ph_ClusterSize7x11Lr3;   //!
   TBranch        *b_Data_ph_ClusterSize3x7Lr3;   //!
   TBranch        *b_Data_ph_ClusterSize5x5Lr3;   //!
   TBranch        *b_Data_ph_ClusterSize7x11Lr1;   //!
   TBranch        *b_Data_ph_ClusterSize3x7Lr1;   //!
   TBranch        *b_Data_ph_ClusterSize5x5Lr1;   //!
   TBranch        *b_Data_ph_clusterCellsLr2E7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Eta7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Phi7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr2E3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Eta3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Phi3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr2E5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Eta5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Phi5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr1E7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Eta7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Phi7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr1E3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Eta3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Phi3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr1E5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Eta5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Phi5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr3E7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Eta7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Phi7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr3E3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Eta3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Phi3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr3E5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Eta5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Phi5x5;   //!
   TBranch        *b_Data_l1_Pt;   //!
   TBranch        *b_Data_l1_Eta;   //!
   TBranch        *b_Data_l1_Phi;   //!
   TBranch        *b_Data_l1_E;   //!
   TBranch        *b_Data_l1_Charge;   //!
   TBranch        *b_Data_l2_Pt;   //!
   TBranch        *b_Data_l2_Eta;   //!
   TBranch        *b_Data_l2_Phi;   //!
   TBranch        *b_Data_l2_E;   //!
   TBranch        *b_Data_l2_Charge;   //!
   TBranch        *b_Data_l1_SF;   //!
   TBranch        *b_Data_l2_SF;   //!
   TBranch        *b_Data_pu_wgt;   //!
   TBranch        *b_Data_Mu;   //!
   TBranch        *b_Data_mc_wgt;   //!
   TBranch        *b_Data_mc_xsec;   //!
   TBranch        *b_Data_isZeey;   //!
   TBranch        *b_Data_isZmumuy;   //!
   TBranch        *b_Data_M_ee;   //!
   TBranch        *b_Data_M_eey;   //!
   TBranch        *b_Data_M_mumu;   //!
   TBranch        *b_Data_M_mumuy;   //!

   tree(TTree *tree=0);
   virtual ~tree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   float Energy(int eta, int phi, std::vector<float> *clusterEnergy);
};

#endif

#ifdef tree_cxx


float tree::Energy(int eta, int phi, std::vector<float> *clusterEnergy) 
{
    int etaSize = 7;
    int phiSize = 11;
    
    int etaMin = etaSize - (etaSize + eta)/2;
    int etaMax = etaSize - (etaSize - eta)/2;
    int phiMin = phiSize - (phiSize + phi)/2;
    int phiMax = phiSize - (phiSize - phi)/2;
    float sumEE = 0;
    
    for ( int e = etaMin; e < etaMax; e ++ ) {
        for ( int p = phiMin; p < phiMax; p ++ ) {
            sumEE += clusterEnergy->at(p+phiSize*e);
        }
    }
        return sumEE;
}

tree::tree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Tree_matched_Erased.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Tree_matched_Erased.root");
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

tree::~tree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t tree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t tree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void tree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   MC_ph_clusterCellsLr2E7x11 = 0;
   MC_ph_clusterCellsLr2Eta7x11 = 0;
   MC_ph_clusterCellsLr2Phi7x11 = 0;
   MC_ph_clusterCellsLr2E3x7 = 0;
   MC_ph_clusterCellsLr2Eta3x7 = 0;
   MC_ph_clusterCellsLr2Phi3x7 = 0;
   MC_ph_clusterCellsLr2E5x5 = 0;
   MC_ph_clusterCellsLr2Eta5x5 = 0;
   MC_ph_clusterCellsLr2Phi5x5 = 0;
   MC_ph_clusterCellsLr1E7x11 = 0;
   MC_ph_clusterCellsLr1Eta7x11 = 0;
   MC_ph_clusterCellsLr1Phi7x11 = 0;
   MC_ph_clusterCellsLr1E3x7 = 0;
   MC_ph_clusterCellsLr1Eta3x7 = 0;
   MC_ph_clusterCellsLr1Phi3x7 = 0;
   MC_ph_clusterCellsLr1E5x5 = 0;
   MC_ph_clusterCellsLr1Eta5x5 = 0;
   MC_ph_clusterCellsLr1Phi5x5 = 0;
   MC_ph_clusterCellsLr3E7x11 = 0;
   MC_ph_clusterCellsLr3Eta7x11 = 0;
   MC_ph_clusterCellsLr3Phi7x11 = 0;
   MC_ph_clusterCellsLr3E3x7 = 0;
   MC_ph_clusterCellsLr3Eta3x7 = 0;
   MC_ph_clusterCellsLr3Phi3x7 = 0;
   MC_ph_clusterCellsLr3E5x5 = 0;
   MC_ph_clusterCellsLr3Eta5x5 = 0;
   MC_ph_clusterCellsLr3Phi5x5 = 0;
   MC_M_ee = 0;
   MC_M_eey = 0;
   MC_M_mumu = 0;
   MC_M_mumuy = 0;
   Data_ph_clusterCellsLr2E7x11 = 0;
   Data_ph_clusterCellsLr2Eta7x11 = 0;
   Data_ph_clusterCellsLr2Phi7x11 = 0;
   Data_ph_clusterCellsLr2E3x7 = 0;
   Data_ph_clusterCellsLr2Eta3x7 = 0;
   Data_ph_clusterCellsLr2Phi3x7 = 0;
   Data_ph_clusterCellsLr2E5x5 = 0;
   Data_ph_clusterCellsLr2Eta5x5 = 0;
   Data_ph_clusterCellsLr2Phi5x5 = 0;
   Data_ph_clusterCellsLr1E7x11 = 0;
   Data_ph_clusterCellsLr1Eta7x11 = 0;
   Data_ph_clusterCellsLr1Phi7x11 = 0;
   Data_ph_clusterCellsLr1E3x7 = 0;
   Data_ph_clusterCellsLr1Eta3x7 = 0;
   Data_ph_clusterCellsLr1Phi3x7 = 0;
   Data_ph_clusterCellsLr1E5x5 = 0;
   Data_ph_clusterCellsLr1Eta5x5 = 0;
   Data_ph_clusterCellsLr1Phi5x5 = 0;
   Data_ph_clusterCellsLr3E7x11 = 0;
   Data_ph_clusterCellsLr3Eta7x11 = 0;
   Data_ph_clusterCellsLr3Phi7x11 = 0;
   Data_ph_clusterCellsLr3E3x7 = 0;
   Data_ph_clusterCellsLr3Eta3x7 = 0;
   Data_ph_clusterCellsLr3Phi3x7 = 0;
   Data_ph_clusterCellsLr3E5x5 = 0;
   Data_ph_clusterCellsLr3Eta5x5 = 0;
   Data_ph_clusterCellsLr3Phi5x5 = 0;
   Data_M_ee = 0;
   Data_M_eey = 0;
   Data_M_mumu = 0;
   Data_M_mumuy = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("MC_ph_Pt", &MC_ph_Pt, &b_MC_ph_Pt);
   fChain->SetBranchAddress("MC_ph_Eta", &MC_ph_Eta, &b_MC_ph_Eta);
   fChain->SetBranchAddress("MC_ph_Phi", &MC_ph_Phi, &b_MC_ph_Phi);
   fChain->SetBranchAddress("MC_ph_E", &MC_ph_E, &b_MC_ph_E);
   fChain->SetBranchAddress("MC_ph_topoetcone20", &MC_ph_topoetcone20, &b_MC_ph_topoetcone20);
   fChain->SetBranchAddress("MC_ph_topoetcone30", &MC_ph_topoetcone30, &b_MC_ph_topoetcone30);
   fChain->SetBranchAddress("MC_ph_topoetcone40", &MC_ph_topoetcone40, &b_MC_ph_topoetcone40);
   fChain->SetBranchAddress("MC_ph_ptcone20", &MC_ph_ptcone20, &b_MC_ph_ptcone20);
   fChain->SetBranchAddress("MC_ph_ptcone30", &MC_ph_ptcone30, &b_MC_ph_ptcone30);
   fChain->SetBranchAddress("MC_ph_ptcone40", &MC_ph_ptcone40, &b_MC_ph_ptcone40);
   fChain->SetBranchAddress("MC_ph_etcone20", &MC_ph_etcone20, &b_MC_ph_etcone20);
   fChain->SetBranchAddress("MC_ph_etcone30", &MC_ph_etcone30, &b_MC_ph_etcone30);
   fChain->SetBranchAddress("MC_ph_etcone40", &MC_ph_etcone40, &b_MC_ph_etcone40);
   fChain->SetBranchAddress("MC_ph_ptvarcone20", &MC_ph_ptvarcone20, &b_MC_ph_ptvarcone20);
   fChain->SetBranchAddress("MC_ph_ptvarcone30", &MC_ph_ptvarcone30, &b_MC_ph_ptvarcone30);
   fChain->SetBranchAddress("MC_ph_ptvarcone40", &MC_ph_ptvarcone40, &b_MC_ph_ptvarcone40);
   fChain->SetBranchAddress("MC_ph_isIsoLooseWP", &MC_ph_isIsoLooseWP, &b_MC_ph_isIsoLooseWP);
   fChain->SetBranchAddress("MC_ph_isIsoTightWP", &MC_ph_isIsoTightWP, &b_MC_ph_isIsoTightWP);
   fChain->SetBranchAddress("MC_ph_isIsoTightCaloOnlyWP", &MC_ph_isIsoTightCaloOnlyWP, &b_MC_ph_isIsoTightCaloOnlyWP);
   fChain->SetBranchAddress("MC_ph_Conversion", &MC_ph_Conversion, &b_MC_ph_Conversion);
   fChain->SetBranchAddress("MC_ph_Ethad", &MC_ph_Ethad, &b_MC_ph_Ethad);
   fChain->SetBranchAddress("MC_ph_Ethad1", &MC_ph_Ethad1, &b_MC_ph_Ethad1);
   fChain->SetBranchAddress("MC_ph_Rhad1", &MC_ph_Rhad1, &b_MC_ph_Rhad1);
   fChain->SetBranchAddress("MC_ph_Rhad", &MC_ph_Rhad, &b_MC_ph_Rhad);
   fChain->SetBranchAddress("MC_ph_E011", &MC_ph_E011, &b_MC_ph_E011);
   fChain->SetBranchAddress("MC_ph_E132", &MC_ph_E132, &b_MC_ph_E132);
   fChain->SetBranchAddress("MC_ph_E237", &MC_ph_E237, &b_MC_ph_E237);
   fChain->SetBranchAddress("MC_ph_E277", &MC_ph_E277, &b_MC_ph_E277);
   fChain->SetBranchAddress("MC_ph_Reta", &MC_ph_Reta, &b_MC_ph_Reta);
   fChain->SetBranchAddress("MC_ph_Rphi", &MC_ph_Rphi, &b_MC_ph_Rphi);
   fChain->SetBranchAddress("MC_ph_Weta1", &MC_ph_Weta1, &b_MC_ph_Weta1);
   fChain->SetBranchAddress("MC_ph_Weta2", &MC_ph_Weta2, &b_MC_ph_Weta2);
   fChain->SetBranchAddress("MC_ph_f1", &MC_ph_f1, &b_MC_ph_f1);
   fChain->SetBranchAddress("MC_ph_f3", &MC_ph_f3, &b_MC_ph_f3);
   fChain->SetBranchAddress("MC_ph_f3core", &MC_ph_f3core, &b_MC_ph_f3core);
   fChain->SetBranchAddress("MC_ph_fracs1", &MC_ph_fracs1, &b_MC_ph_fracs1);
   fChain->SetBranchAddress("MC_ph_Wstot1", &MC_ph_Wstot1, &b_MC_ph_Wstot1);
   fChain->SetBranchAddress("MC_ph_deltaE", &MC_ph_deltaE, &b_MC_ph_deltaE);
   fChain->SetBranchAddress("MC_ph_Eratio", &MC_ph_Eratio, &b_MC_ph_Eratio);
   fChain->SetBranchAddress("MC_ph_E2tsts1", &MC_ph_E2tsts1, &b_MC_ph_E2tsts1);
   fChain->SetBranchAddress("MC_ph_Emins1", &MC_ph_Emins1, &b_MC_ph_Emins1);
   fChain->SetBranchAddress("MC_ph_Emaxs1", &MC_ph_Emaxs1, &b_MC_ph_Emaxs1);
   fChain->SetBranchAddress("MC_ph_ClusterSize7x11Lr2", &MC_ph_ClusterSize7x11Lr2, &b_MC_ph_ClusterSize7x11Lr2);
   fChain->SetBranchAddress("MC_ph_ClusterSize3x7Lr2", &MC_ph_ClusterSize3x7Lr2, &b_MC_ph_ClusterSize3x7Lr2);
   fChain->SetBranchAddress("MC_ph_ClusterSize5x5Lr2", &MC_ph_ClusterSize5x5Lr2, &b_MC_ph_ClusterSize5x5Lr2);
   fChain->SetBranchAddress("MC_ph_ClusterSize7x11Lr3", &MC_ph_ClusterSize7x11Lr3, &b_MC_ph_ClusterSize7x11Lr3);
   fChain->SetBranchAddress("MC_ph_ClusterSize3x7Lr3", &MC_ph_ClusterSize3x7Lr3, &b_MC_ph_ClusterSize3x7Lr3);
   fChain->SetBranchAddress("MC_ph_ClusterSize5x5Lr3", &MC_ph_ClusterSize5x5Lr3, &b_MC_ph_ClusterSize5x5Lr3);
   fChain->SetBranchAddress("MC_ph_ClusterSize7x11Lr1", &MC_ph_ClusterSize7x11Lr1, &b_MC_ph_ClusterSize7x11Lr1);
   fChain->SetBranchAddress("MC_ph_ClusterSize3x7Lr1", &MC_ph_ClusterSize3x7Lr1, &b_MC_ph_ClusterSize3x7Lr1);
   fChain->SetBranchAddress("MC_ph_ClusterSize5x5Lr1", &MC_ph_ClusterSize5x5Lr1, &b_MC_ph_ClusterSize5x5Lr1);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2E7x11", &MC_ph_clusterCellsLr2E7x11, &b_MC_ph_clusterCellsLr2E7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2Eta7x11", &MC_ph_clusterCellsLr2Eta7x11, &b_MC_ph_clusterCellsLr2Eta7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2Phi7x11", &MC_ph_clusterCellsLr2Phi7x11, &b_MC_ph_clusterCellsLr2Phi7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2E3x7", &MC_ph_clusterCellsLr2E3x7, &b_MC_ph_clusterCellsLr2E3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2Eta3x7", &MC_ph_clusterCellsLr2Eta3x7, &b_MC_ph_clusterCellsLr2Eta3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2Phi3x7", &MC_ph_clusterCellsLr2Phi3x7, &b_MC_ph_clusterCellsLr2Phi3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2E5x5", &MC_ph_clusterCellsLr2E5x5, &b_MC_ph_clusterCellsLr2E5x5);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2Eta5x5", &MC_ph_clusterCellsLr2Eta5x5, &b_MC_ph_clusterCellsLr2Eta5x5);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr2Phi5x5", &MC_ph_clusterCellsLr2Phi5x5, &b_MC_ph_clusterCellsLr2Phi5x5);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1E7x11", &MC_ph_clusterCellsLr1E7x11, &b_MC_ph_clusterCellsLr1E7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1Eta7x11", &MC_ph_clusterCellsLr1Eta7x11, &b_MC_ph_clusterCellsLr1Eta7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1Phi7x11", &MC_ph_clusterCellsLr1Phi7x11, &b_MC_ph_clusterCellsLr1Phi7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1E3x7", &MC_ph_clusterCellsLr1E3x7, &b_MC_ph_clusterCellsLr1E3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1Eta3x7", &MC_ph_clusterCellsLr1Eta3x7, &b_MC_ph_clusterCellsLr1Eta3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1Phi3x7", &MC_ph_clusterCellsLr1Phi3x7, &b_MC_ph_clusterCellsLr1Phi3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1E5x5", &MC_ph_clusterCellsLr1E5x5, &b_MC_ph_clusterCellsLr1E5x5);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1Eta5x5", &MC_ph_clusterCellsLr1Eta5x5, &b_MC_ph_clusterCellsLr1Eta5x5);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr1Phi5x5", &MC_ph_clusterCellsLr1Phi5x5, &b_MC_ph_clusterCellsLr1Phi5x5);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3E7x11", &MC_ph_clusterCellsLr3E7x11, &b_MC_ph_clusterCellsLr3E7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3Eta7x11", &MC_ph_clusterCellsLr3Eta7x11, &b_MC_ph_clusterCellsLr3Eta7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3Phi7x11", &MC_ph_clusterCellsLr3Phi7x11, &b_MC_ph_clusterCellsLr3Phi7x11);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3E3x7", &MC_ph_clusterCellsLr3E3x7, &b_MC_ph_clusterCellsLr3E3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3Eta3x7", &MC_ph_clusterCellsLr3Eta3x7, &b_MC_ph_clusterCellsLr3Eta3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3Phi3x7", &MC_ph_clusterCellsLr3Phi3x7, &b_MC_ph_clusterCellsLr3Phi3x7);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3E5x5", &MC_ph_clusterCellsLr3E5x5, &b_MC_ph_clusterCellsLr3E5x5);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3Eta5x5", &MC_ph_clusterCellsLr3Eta5x5, &b_MC_ph_clusterCellsLr3Eta5x5);
   fChain->SetBranchAddress("MC_ph_clusterCellsLr3Phi5x5", &MC_ph_clusterCellsLr3Phi5x5, &b_MC_ph_clusterCellsLr3Phi5x5);
   fChain->SetBranchAddress("MC_l1_Pt", &MC_l1_Pt, &b_MC_l1_Pt);
   fChain->SetBranchAddress("MC_l1_Eta", &MC_l1_Eta, &b_MC_l1_Eta);
   fChain->SetBranchAddress("MC_l1_Phi", &MC_l1_Phi, &b_MC_l1_Phi);
   fChain->SetBranchAddress("MC_l1_E", &MC_l1_E, &b_MC_l1_E);
   fChain->SetBranchAddress("MC_l1_Charge", &MC_l1_Charge, &b_MC_l1_Charge);
   fChain->SetBranchAddress("MC_l2_Pt", &MC_l2_Pt, &b_MC_l2_Pt);
   fChain->SetBranchAddress("MC_l2_Eta", &MC_l2_Eta, &b_MC_l2_Eta);
   fChain->SetBranchAddress("MC_l2_Phi", &MC_l2_Phi, &b_MC_l2_Phi);
   fChain->SetBranchAddress("MC_l2_E", &MC_l2_E, &b_MC_l2_E);
   fChain->SetBranchAddress("MC_l2_Charge", &MC_l2_Charge, &b_MC_l2_Charge);
   fChain->SetBranchAddress("MC_l1_SF", &MC_l1_SF, &b_MC_l1_SF);
   fChain->SetBranchAddress("MC_l2_SF", &MC_l2_SF, &b_MC_l2_SF);
   fChain->SetBranchAddress("MC_pu_wgt", &MC_pu_wgt, &b_MC_pu_wgt);
   fChain->SetBranchAddress("MC_Mu", &MC_Mu, &b_MC_Mu);
   fChain->SetBranchAddress("MC_mc_wgt", &MC_mc_wgt, &b_MC_mc_wgt);
   fChain->SetBranchAddress("MC_mc_xsec", &MC_mc_xsec, &b_MC_mc_xsec);
   fChain->SetBranchAddress("MC_isZeey", &MC_isZeey, &b_MC_isZeey);
   fChain->SetBranchAddress("MC_isZmumuy", &MC_isZmumuy, &b_MC_isZmumuy);
   fChain->SetBranchAddress("MC_M_ee", &MC_M_ee, &b_MC_M_ee);
   fChain->SetBranchAddress("MC_M_eey", &MC_M_eey, &b_MC_M_eey);
   fChain->SetBranchAddress("MC_M_mumu", &MC_M_mumu, &b_MC_M_mumu);
   fChain->SetBranchAddress("MC_M_mumuy", &MC_M_mumuy, &b_MC_M_mumuy);
   fChain->SetBranchAddress("MC_i", &MC_i, &b_MC_i);
   fChain->SetBranchAddress("Data_i", &Data_i, &b_Data_i);
   fChain->SetBranchAddress("Data_ph_Pt", &Data_ph_Pt, &b_Data_ph_Pt);
   fChain->SetBranchAddress("Data_ph_Eta", &Data_ph_Eta, &b_Data_ph_Eta);
   fChain->SetBranchAddress("Data_ph_Phi", &Data_ph_Phi, &b_Data_ph_Phi);
   fChain->SetBranchAddress("Data_ph_E", &Data_ph_E, &b_Data_ph_E);
   fChain->SetBranchAddress("Data_ph_topoetcone20", &Data_ph_topoetcone20, &b_Data_ph_topoetcone20);
   fChain->SetBranchAddress("Data_ph_topoetcone30", &Data_ph_topoetcone30, &b_Data_ph_topoetcone30);
   fChain->SetBranchAddress("Data_ph_topoetcone40", &Data_ph_topoetcone40, &b_Data_ph_topoetcone40);
   fChain->SetBranchAddress("Data_ph_ptcone20", &Data_ph_ptcone20, &b_Data_ph_ptcone20);
   fChain->SetBranchAddress("Data_ph_ptcone30", &Data_ph_ptcone30, &b_Data_ph_ptcone30);
   fChain->SetBranchAddress("Data_ph_ptcone40", &Data_ph_ptcone40, &b_Data_ph_ptcone40);
   fChain->SetBranchAddress("Data_ph_etcone20", &Data_ph_etcone20, &b_Data_ph_etcone20);
   fChain->SetBranchAddress("Data_ph_etcone30", &Data_ph_etcone30, &b_Data_ph_etcone30);
   fChain->SetBranchAddress("Data_ph_etcone40", &Data_ph_etcone40, &b_Data_ph_etcone40);
   fChain->SetBranchAddress("Data_ph_ptvarcone20", &Data_ph_ptvarcone20, &b_Data_ph_ptvarcone20);
   fChain->SetBranchAddress("Data_ph_ptvarcone30", &Data_ph_ptvarcone30, &b_Data_ph_ptvarcone30);
   fChain->SetBranchAddress("Data_ph_ptvarcone40", &Data_ph_ptvarcone40, &b_Data_ph_ptvarcone40);
   fChain->SetBranchAddress("Data_ph_isIsoLooseWP", &Data_ph_isIsoLooseWP, &b_Data_ph_isIsoLooseWP);
   fChain->SetBranchAddress("Data_ph_isIsoTightWP", &Data_ph_isIsoTightWP, &b_Data_ph_isIsoTightWP);
   fChain->SetBranchAddress("Data_ph_isIsoTightCaloOnlyWP", &Data_ph_isIsoTightCaloOnlyWP, &b_Data_ph_isIsoTightCaloOnlyWP);
   fChain->SetBranchAddress("Data_ph_Conversion", &Data_ph_Conversion, &b_Data_ph_Conversion);
   fChain->SetBranchAddress("Data_ph_Ethad", &Data_ph_Ethad, &b_Data_ph_Ethad);
   fChain->SetBranchAddress("Data_ph_Ethad1", &Data_ph_Ethad1, &b_Data_ph_Ethad1);
   fChain->SetBranchAddress("Data_ph_Rhad1", &Data_ph_Rhad1, &b_Data_ph_Rhad1);
   fChain->SetBranchAddress("Data_ph_Rhad", &Data_ph_Rhad, &b_Data_ph_Rhad);
   fChain->SetBranchAddress("Data_ph_E011", &Data_ph_E011, &b_Data_ph_E011);
   fChain->SetBranchAddress("Data_ph_E132", &Data_ph_E132, &b_Data_ph_E132);
   fChain->SetBranchAddress("Data_ph_E237", &Data_ph_E237, &b_Data_ph_E237);
   fChain->SetBranchAddress("Data_ph_E277", &Data_ph_E277, &b_Data_ph_E277);
   fChain->SetBranchAddress("Data_ph_Reta", &Data_ph_Reta, &b_Data_ph_Reta);
   fChain->SetBranchAddress("Data_ph_Rphi", &Data_ph_Rphi, &b_Data_ph_Rphi);
   fChain->SetBranchAddress("Data_ph_Weta1", &Data_ph_Weta1, &b_Data_ph_Weta1);
   fChain->SetBranchAddress("Data_ph_Weta2", &Data_ph_Weta2, &b_Data_ph_Weta2);
   fChain->SetBranchAddress("Data_ph_f1", &Data_ph_f1, &b_Data_ph_f1);
   fChain->SetBranchAddress("Data_ph_f3", &Data_ph_f3, &b_Data_ph_f3);
   fChain->SetBranchAddress("Data_ph_f3core", &Data_ph_f3core, &b_Data_ph_f3core);
   fChain->SetBranchAddress("Data_ph_fracs1", &Data_ph_fracs1, &b_Data_ph_fracs1);
   fChain->SetBranchAddress("Data_ph_Wstot1", &Data_ph_Wstot1, &b_Data_ph_Wstot1);
   fChain->SetBranchAddress("Data_ph_deltaE", &Data_ph_deltaE, &b_Data_ph_deltaE);
   fChain->SetBranchAddress("Data_ph_Eratio", &Data_ph_Eratio, &b_Data_ph_Eratio);
   fChain->SetBranchAddress("Data_ph_E2tsts1", &Data_ph_E2tsts1, &b_Data_ph_E2tsts1);
   fChain->SetBranchAddress("Data_ph_Emins1", &Data_ph_Emins1, &b_Data_ph_Emins1);
   fChain->SetBranchAddress("Data_ph_Emaxs1", &Data_ph_Emaxs1, &b_Data_ph_Emaxs1);
   fChain->SetBranchAddress("Data_ph_ClusterSize7x11Lr2", &Data_ph_ClusterSize7x11Lr2, &b_Data_ph_ClusterSize7x11Lr2);
   fChain->SetBranchAddress("Data_ph_ClusterSize3x7Lr2", &Data_ph_ClusterSize3x7Lr2, &b_Data_ph_ClusterSize3x7Lr2);
   fChain->SetBranchAddress("Data_ph_ClusterSize5x5Lr2", &Data_ph_ClusterSize5x5Lr2, &b_Data_ph_ClusterSize5x5Lr2);
   fChain->SetBranchAddress("Data_ph_ClusterSize7x11Lr3", &Data_ph_ClusterSize7x11Lr3, &b_Data_ph_ClusterSize7x11Lr3);
   fChain->SetBranchAddress("Data_ph_ClusterSize3x7Lr3", &Data_ph_ClusterSize3x7Lr3, &b_Data_ph_ClusterSize3x7Lr3);
   fChain->SetBranchAddress("Data_ph_ClusterSize5x5Lr3", &Data_ph_ClusterSize5x5Lr3, &b_Data_ph_ClusterSize5x5Lr3);
   fChain->SetBranchAddress("Data_ph_ClusterSize7x11Lr1", &Data_ph_ClusterSize7x11Lr1, &b_Data_ph_ClusterSize7x11Lr1);
   fChain->SetBranchAddress("Data_ph_ClusterSize3x7Lr1", &Data_ph_ClusterSize3x7Lr1, &b_Data_ph_ClusterSize3x7Lr1);
   fChain->SetBranchAddress("Data_ph_ClusterSize5x5Lr1", &Data_ph_ClusterSize5x5Lr1, &b_Data_ph_ClusterSize5x5Lr1);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2E7x11", &Data_ph_clusterCellsLr2E7x11, &b_Data_ph_clusterCellsLr2E7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2Eta7x11", &Data_ph_clusterCellsLr2Eta7x11, &b_Data_ph_clusterCellsLr2Eta7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2Phi7x11", &Data_ph_clusterCellsLr2Phi7x11, &b_Data_ph_clusterCellsLr2Phi7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2E3x7", &Data_ph_clusterCellsLr2E3x7, &b_Data_ph_clusterCellsLr2E3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2Eta3x7", &Data_ph_clusterCellsLr2Eta3x7, &b_Data_ph_clusterCellsLr2Eta3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2Phi3x7", &Data_ph_clusterCellsLr2Phi3x7, &b_Data_ph_clusterCellsLr2Phi3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2E5x5", &Data_ph_clusterCellsLr2E5x5, &b_Data_ph_clusterCellsLr2E5x5);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2Eta5x5", &Data_ph_clusterCellsLr2Eta5x5, &b_Data_ph_clusterCellsLr2Eta5x5);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr2Phi5x5", &Data_ph_clusterCellsLr2Phi5x5, &b_Data_ph_clusterCellsLr2Phi5x5);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1E7x11", &Data_ph_clusterCellsLr1E7x11, &b_Data_ph_clusterCellsLr1E7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1Eta7x11", &Data_ph_clusterCellsLr1Eta7x11, &b_Data_ph_clusterCellsLr1Eta7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1Phi7x11", &Data_ph_clusterCellsLr1Phi7x11, &b_Data_ph_clusterCellsLr1Phi7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1E3x7", &Data_ph_clusterCellsLr1E3x7, &b_Data_ph_clusterCellsLr1E3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1Eta3x7", &Data_ph_clusterCellsLr1Eta3x7, &b_Data_ph_clusterCellsLr1Eta3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1Phi3x7", &Data_ph_clusterCellsLr1Phi3x7, &b_Data_ph_clusterCellsLr1Phi3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1E5x5", &Data_ph_clusterCellsLr1E5x5, &b_Data_ph_clusterCellsLr1E5x5);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1Eta5x5", &Data_ph_clusterCellsLr1Eta5x5, &b_Data_ph_clusterCellsLr1Eta5x5);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr1Phi5x5", &Data_ph_clusterCellsLr1Phi5x5, &b_Data_ph_clusterCellsLr1Phi5x5);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3E7x11", &Data_ph_clusterCellsLr3E7x11, &b_Data_ph_clusterCellsLr3E7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3Eta7x11", &Data_ph_clusterCellsLr3Eta7x11, &b_Data_ph_clusterCellsLr3Eta7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3Phi7x11", &Data_ph_clusterCellsLr3Phi7x11, &b_Data_ph_clusterCellsLr3Phi7x11);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3E3x7", &Data_ph_clusterCellsLr3E3x7, &b_Data_ph_clusterCellsLr3E3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3Eta3x7", &Data_ph_clusterCellsLr3Eta3x7, &b_Data_ph_clusterCellsLr3Eta3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3Phi3x7", &Data_ph_clusterCellsLr3Phi3x7, &b_Data_ph_clusterCellsLr3Phi3x7);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3E5x5", &Data_ph_clusterCellsLr3E5x5, &b_Data_ph_clusterCellsLr3E5x5);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3Eta5x5", &Data_ph_clusterCellsLr3Eta5x5, &b_Data_ph_clusterCellsLr3Eta5x5);
   fChain->SetBranchAddress("Data_ph_clusterCellsLr3Phi5x5", &Data_ph_clusterCellsLr3Phi5x5, &b_Data_ph_clusterCellsLr3Phi5x5);
   fChain->SetBranchAddress("Data_l1_Pt", &Data_l1_Pt, &b_Data_l1_Pt);
   fChain->SetBranchAddress("Data_l1_Eta", &Data_l1_Eta, &b_Data_l1_Eta);
   fChain->SetBranchAddress("Data_l1_Phi", &Data_l1_Phi, &b_Data_l1_Phi);
   fChain->SetBranchAddress("Data_l1_E", &Data_l1_E, &b_Data_l1_E);
   fChain->SetBranchAddress("Data_l1_Charge", &Data_l1_Charge, &b_Data_l1_Charge);
   fChain->SetBranchAddress("Data_l2_Pt", &Data_l2_Pt, &b_Data_l2_Pt);
   fChain->SetBranchAddress("Data_l2_Eta", &Data_l2_Eta, &b_Data_l2_Eta);
   fChain->SetBranchAddress("Data_l2_Phi", &Data_l2_Phi, &b_Data_l2_Phi);
   fChain->SetBranchAddress("Data_l2_E", &Data_l2_E, &b_Data_l2_E);
   fChain->SetBranchAddress("Data_l2_Charge", &Data_l2_Charge, &b_Data_l2_Charge);
   fChain->SetBranchAddress("Data_l1_SF", &Data_l1_SF, &b_Data_l1_SF);
   fChain->SetBranchAddress("Data_l2_SF", &Data_l2_SF, &b_Data_l2_SF);
   fChain->SetBranchAddress("Data_pu_wgt", &Data_pu_wgt, &b_Data_pu_wgt);
   fChain->SetBranchAddress("Data_Mu", &Data_Mu, &b_Data_Mu);
   fChain->SetBranchAddress("Data_mc_wgt", &Data_mc_wgt, &b_Data_mc_wgt);
   fChain->SetBranchAddress("Data_mc_xsec", &Data_mc_xsec, &b_Data_mc_xsec);
   fChain->SetBranchAddress("Data_isZeey", &Data_isZeey, &b_Data_isZeey);
   fChain->SetBranchAddress("Data_isZmumuy", &Data_isZmumuy, &b_Data_isZmumuy);
   fChain->SetBranchAddress("Data_M_ee", &Data_M_ee, &b_Data_M_ee);
   fChain->SetBranchAddress("Data_M_eey", &Data_M_eey, &b_Data_M_eey);
   fChain->SetBranchAddress("Data_M_mumu", &Data_M_mumu, &b_Data_M_mumu);
   fChain->SetBranchAddress("Data_M_mumuy", &Data_M_mumuy, &b_Data_M_mumuy);
   Notify();
}

Bool_t tree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void tree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t tree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef tree_cxx
