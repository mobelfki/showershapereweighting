/////////////////////////////////////////
// Attempts to compute the 3D reweighting by doing data-MC matching 
////////////////////////////////////////

#include "var.h"


int NTUP()
{

	TString type = "NCon";
	bool doRew = false;
	vector<TString> MC_names,Data_names;

	MC_names.push_back("/eos/user/m/mobelfki/ZllyAthDerivation2/NEW/user.mobelfki.mc16d.301535.RadiativeZ_SF.NTUP.e3952_s3126_r10201_r10210_ANALYSIS/");
	MC_names.push_back("/eos/user/m/mobelfki/ZllyAthDerivation2/NEW/user.mobelfki.mc16d.301899.RadiativeZ_SF2.NTUP.e3952_s3126_r10201_r10210_ANALYSIS/");
	MC_names.push_back("/eos/user/m/mobelfki/ZllyAthDerivation2/NEW/user.mobelfki.mc16d.301900.RadiativeZ_SF2.NTUP.e3952_s3126_r10201_r10210_ANALYSIS/");

	MC_names.push_back("/eos/user/m/mobelfki/ZllyAthDerivation2/NEW/user.mobelfki.mc16d.301536.RadiativeZ_SF2.NTUP.e3952_s3126_r10201_r10210_ANALYSIS/");
	MC_names.push_back("/eos/user/m/mobelfki/ZllyAthDerivation2/NEW/user.mobelfki.mc16d.301902.RadiativeZ_SF2.NTUP.e3952_s3126_r10201_r10210_ANALYSIS/");
	MC_names.push_back("/eos/user/m/mobelfki/ZllyAthDerivation2/NEW/user.mobelfki.mc16d.301903.RadiativeZ_SF2.NTUP.e3952_s3126_r10201_r10210_ANALYSIS/");

	Data_names.push_back("/eos/user/m/mobelfki/ZllyAthDerivation2/user.mobelfki.data2017.RadiativeZ.NTUP.e3952_s3126_r10201_r10210_ANALYSIS/");
	
	TChain* MC_chain = LoadMCChain(MC_names);
	TChain* Data_chain = LoadDataChain(Data_names);
	
	Loop(MC_chain,Data_chain,type,doRew);
	return 1;
}


void Loop(TChain* MC_chain, TChain* Data_chain,TString type,bool doRew)
{
	int MC_nentry = MC_chain->GetEntriesFast();
	int Data_nentry = Data_chain->GetEntriesFast();

	std::cout<<" The totale number of entries is : "<< MC_nentry * Data_nentry <<std::endl;
	std::cout<<" The totale number of MC entries is : "<< MC_nentry <<std::endl;
	std::cout<<" The totale number of Data entries is : "<< Data_nentry <<std::endl;

	int nbtotal = 0;
	HistoInit();

	std::cout<<"Start looping ..."<<std::endl;
	for(int i = 0; i<1; i++)
	{
	TString target = _target[i].c_str();
	
	float lumi = getXsec(MC_chain);
	if(doRew){
		//InitRew(target,type);
	}

	Init(target,doRew);
	std::cout<<"Target : "<< target <<std::endl;
	
	Ana(MC_chain, Data_chain, target, doRew);
	Fin(lumi);
	}
}
void HistoInit()
{
	for(unsigned int j = 0; j<nType; j++) {
	for(unsigned int i = 0; i<nEtaBins; i++) {
	for(unsigned int k = 0; k<77; k++) {

			CellCorrection[k][i][j] = new TProfile(Form("Energy_Cell_%i_%s_Eta_%1.2f_%1.2f",k+1,Type[j].c_str(),etaLimits[i], etaLimits[i+1]), Form("Energy_Cell_%i_%s_Eta_%1.2f_%1.2f",k+1,Type[j].c_str(),etaLimits[i], etaLimits[i+1]), 20,-1.,1,-10.,10.);

	}
	}
	}
}
void Init(TString target, bool doRew)
{
	for(unsigned int j = 0; j<nType; j++) {
	for(unsigned int i = 0; i<nEtaBins; i++) {
	for(unsigned int k = 0; k<77; k++)
	{
			CellCorrection[k][i][j]->Reset();
	}
	}
	}
	
	TString outputname = "output.";
	outputname += target;
	if(doRew)
	{
		outputname += ".Rewieghted";	
	}	
	outputname += ".root";
	std::cout<<"The output file will be : "<<outputname<<std::endl;
	_out = new TFile(outputname,"RECREATE");
	
	std::cout<<"Initialization For "<< target <<" Done!"<<std::endl;
}

void Fin(float lumi)
{
	for( unsigned int j = 0; j<nType; j++) {
	for(unsigned int i = 0; i<nEtaBins; i++) {
	for(unsigned int k = 0; k<77; k++)
	{

			CellCorrection[k][i][j]->SetDirectory(_out);
	}
	}
	}
	
	_out->Write();
	//_crr->Write();
	std::cout<<"Fin() : Files is created"<<std::endl;
}

void Ana(TChain* MC, TChain* Data, TString target,bool doRew)
{

  int MC_nentry = MC->GetEntriesFast();
  int Data_nentry = Data->GetEntriesFast();

  std::vector<std::pair <TLorentzVector, int>> MC_vect, Data_vect;

  for(int jentry = 0; jentry<MC_nentry; jentry++)
  {
   	int nb = MC->GetEntry(jentry);

  if(target == "Zeey" && !MC_isZeey)
  {
 	continue;	
  }else if(target == "Zmumuy" && !MC_isZmumuy)
  {
	continue;
  }
   
  if(MC_ph_isIsoTightWP != 1) continue; //select looseWP for photon isolation  
		
  if(!(MC_ph_topoetcone40/MC_ph_Pt < 0.022+2.45 && MC_ph_ptcone20/MC_ph_Pt < 0.05 )) continue; //select looseWP for photon isolation  

  if((MC_ph_clusterCellsLr2E7x11)->size() != clusterSize) continue; //select photon with completed cluster
  double maxE = (MC_ph_clusterCellsLr2E7x11)->at(0);
  int maxK = 0;
  for (int k = 1; k < clusterSize; k++)
  {
	if( maxE < (MC_ph_clusterCellsLr2E7x11)->at(k) )
	{
		maxE = (MC_ph_clusterCellsLr2E7x11)->at(k);
		maxK = k;
	}
  }
 
  if(maxK != 38) continue;

  TLorentzVector MC_gamma;
  MC_gamma.SetPtEtaPhiE(MC_ph_Pt,MC_ph_Eta,MC_ph_Phi,MC_ph_E);

  std::pair <TLorentzVector, int> MC_pair(MC_gamma, jentry);
  MC_vect.push_back(MC_pair);
 }
	std::cout<<"MC Loop finished"<<std::endl;
	std::cout<<MC_vect.size()<<" Photons Selected"<<std::endl;


 for(int kentry = 0; kentry<Data_nentry; kentry++)
  {
   	int nb = Data->GetEntry(kentry);

  if(target == "Zeey" && !Data_isZeey)
  {
 	continue;	
  }else if(target == "Zmumuy" && !Data_isZmumuy)
  {
	continue;
  }
   
  if(Data_ph_isIsoTightWP != 1) continue; //select looseWP for photon isolation  
		
  if(!(Data_ph_topoetcone40/Data_ph_Pt < 0.022+2.45 && Data_ph_ptcone20/Data_ph_Pt < 0.05 )) continue; //select looseWP for photon isolation  

  if((Data_ph_clusterCellsLr2E7x11)->size() != clusterSize) continue; //select photon with completed cluster
  double maxE = (Data_ph_clusterCellsLr2E7x11)->at(0);
  int maxK = 0;
  for (int k = 1; k < clusterSize; k++)
  {
	if( maxE < (Data_ph_clusterCellsLr2E7x11)->at(k) )
	{
		maxE = (Data_ph_clusterCellsLr2E7x11)->at(k);
		maxK = k;
	}
  }
 
  if(maxK != 38) continue;

  TLorentzVector Data_gamma;
  Data_gamma.SetPtEtaPhiE(Data_ph_Pt,Data_ph_Eta,Data_ph_Phi,Data_ph_E);

  std::pair <TLorentzVector, int> Data_pair(Data_gamma, kentry);
  Data_vect.push_back(Data_pair);
 }
	std::cout<<"Data Loop finished"<<std::endl;
	std::cout<<Data_vect.size()<<" Photons Selected"<<std::endl;

	std::cout<<"Start Data-MC matching"<<std::endl;

 std::vector<std::pair <int , int > > i_j;
 for(int i = 0; i<MC_vect.size(); i++) {
	double	drMin = 100;
	int k = -99;
 for(int j = 0; j<Data_vect.size(); j++) {
	
 	double dr = MC_vect[i].first.DeltaR(Data_vect[j].first);
	if (dr > 0.1) continue;
	if (dr < drMin){
		drMin = dr;
		k = j;
	}
 }
	if(k == -99) continue;


	std::pair<int , int > test(MC_vect[i].second,Data_vect[k].second);
	i_j.push_back(test);	
 }
	std::cout<<"Matching finished" <<std::endl;
	std::cout<<i_j.size() << " Data-MC photons matched "<<std::endl;

	for(int m = 0; m<i_j.size(); m++)
	{
	for( int n = m+1; n<i_j.size()-1; n++)
	{
	
		if( i_j[n].second == i_j[m].second) { i_j.erase(i_j.begin()+n); }
	}
	}

	std::cout<<"Erasing finished" <<std::endl;
	std::cout<<i_j.size() << " Data-MC photons matched "<<std::endl;

        TFile* file = new TFile("Tree_matched_Erased.root","RECREATE");
  	TTree *tree = new TTree("tree","matched Data_MC");
	SetMatchedTreeBranches(tree);
	for(int i = 0; i<i_j.size(); i++)
	{

	MC_i = i_j[i].first;
	Data_i = i_j[i].second;

	int nb = MC->GetEntry(i_j[i].first);
	int mb = Data->GetEntry(i_j[i].second);

	float sumE_MC = 0;
	for(int k = 0; k<clusterSize; k++) {
		sumE_MC += (MC_ph_clusterCellsLr2E7x11)->at(k);
	}
	
	float sumE_Data = 0;
	for(int k = 0; k<clusterSize; k++) {
		sumE_Data += (Data_ph_clusterCellsLr2E7x11)->at(k);
	}
	/*
	for(int k = 0; k<clusterSize; k++) {
	 	(MC_ph_clusterCellsLr2E7x11)->at(k) /= sumE_MC;
	}
	for(int k = 0; k<clusterSize; k++) {
	 	(Data_ph_clusterCellsLr2E7x11)->at(k) /= sumE_Data;
	}
	*/

	for (int n = 0; n < nEtaBins; n++) {
  	if ( fabs( (MC_ph_Eta) ) <= etaLimits[n+1]) {
	for(int k = 0; k<clusterSize; k++) {
	
		//std::cout<< n << " " <<k<<" " << (Data_ph_clusterCellsLr2E7x11)->at(k) << " " << (MC_ph_clusterCellsLr2E7x11)->at(k) << std::endl;
		//std::cout<< (Data_ph_clusterCellsLr2E7x11)->at(k)/(MC_ph_clusterCellsLr2E7x11)->at(k) <<std::endl;
		//std::cout<< ((Data_ph_clusterCellsLr2E7x11)->at(k) - (MC_ph_clusterCellsLr2E7x11)->at(k))/(MC_ph_clusterCellsLr2E7x11)->at(k) <<std::endl;

		float alpha = ((Data_ph_clusterCellsLr2E7x11)->at(k) - (MC_ph_clusterCellsLr2E7x11)->at(k))/(MC_ph_clusterCellsLr2E7x11)->at(k);

		CellCorrection[k][n][0]->Fill((MC_ph_clusterCellsLr2E7x11)->at(k)/sumE_MC, alpha);			
		if(MC_ph_Conversion == 0) CellCorrection[k][n][1]->Fill((MC_ph_clusterCellsLr2E7x11)->at(k)/sumE_MC, alpha);		
		if(MC_ph_Conversion != 0) CellCorrection[k][n][2]->Fill((MC_ph_clusterCellsLr2E7x11)->at(k)/sumE_MC, alpha);
	}
	
	break;
	
	}
	}

		tree->Fill();
	
	}
	tree->Write();
	file->Close();

}
