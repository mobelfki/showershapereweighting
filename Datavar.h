#include "TChain.h"
#include "TH1.h"
#include "TTree.h"
#include "TSystemDirectory.h"
#include "TH2.h"
#include <iostream>
#include <string>
#include <fstream>
#include <TSystem.h>
#include <vector>
#include <TFile.h>
#include "TString.h"

using namespace std;

void LinkDataBranches(TChain* fChain);
TChain* LoadDataChain(TString name);

// Declaration of leaf types
   Double_t        Data_ph_Pt;
   Double_t        Data_ph_Eta;
   Double_t        Data_ph_Phi;
   Double_t        Data_ph_E;
   Double_t        Data_ph_topoetcone20;
   Double_t        Data_ph_topoetcone30;
   Double_t        Data_ph_topoetcone40;
   Double_t        Data_ph_ptcone20;
   Double_t        Data_ph_ptcone30;
   Double_t        Data_ph_ptcone40;
   Double_t        Data_ph_etcone20;
   Double_t        Data_ph_etcone30;
   Double_t        Data_ph_etcone40;
   Double_t        Data_ph_ptvarcone20;
   Double_t        Data_ph_ptvarcone30;
   Double_t        Data_ph_ptvarcone40;
   Bool_t          Data_ph_isIsoLooseWP;
   Bool_t          Data_ph_isIsoTightWP;
   Bool_t          Data_ph_isIsoTightCaloOnlyWP;
   Double_t        Data_ph_Conversion;
   Double_t        Data_ph_Ethad;
   Double_t        Data_ph_Ethad1;
   Double_t        Data_ph_Rhad1;
   Double_t        Data_ph_Rhad;
   Double_t        Data_ph_E011;
   Double_t        Data_ph_E132;
   Double_t        Data_ph_E237;
   Double_t        Data_ph_E277;
   Double_t        Data_ph_Reta;
   Double_t        Data_ph_Rphi;
   Double_t        Data_ph_Weta1;
   Double_t        Data_ph_Weta2;
   Double_t        Data_ph_f1;
   Double_t        Data_ph_f3;
   Double_t        Data_ph_f3core;
   Double_t        Data_ph_fracs1;
   Double_t        Data_ph_Wstot1;
   Double_t        Data_ph_deltaE;
   Double_t        Data_ph_Eratio;
   Double_t        Data_ph_E2tsts1;
   Double_t        Data_ph_Emins1;
   Double_t        Data_ph_Emaxs1;
   Int_t           Data_ph_ClusterSize7x11Lr2;
   Int_t           Data_ph_ClusterSize3x7Lr2;
   Int_t           Data_ph_ClusterSize5x5Lr2;
   Int_t           Data_ph_ClusterSize7x11Lr3;
   Int_t           Data_ph_ClusterSize3x7Lr3;
   Int_t           Data_ph_ClusterSize5x5Lr3;
   Int_t           Data_ph_ClusterSize7x11Lr1;
   Int_t           Data_ph_ClusterSize3x7Lr1;
   Int_t           Data_ph_ClusterSize5x5Lr1;
   vector<float>   *Data_ph_clusterCellsLr2E7x11;
   vector<float>   *Data_ph_clusterCellsLr2Eta7x11;
   vector<float>   *Data_ph_clusterCellsLr2Phi7x11;
   vector<float>   *Data_ph_clusterCellsLr2E3x7;
   vector<float>   *Data_ph_clusterCellsLr2Eta3x7;
   vector<float>   *Data_ph_clusterCellsLr2Phi3x7;
   vector<float>   *Data_ph_clusterCellsLr2E5x5;
   vector<float>   *Data_ph_clusterCellsLr2Eta5x5;
   vector<float>   *Data_ph_clusterCellsLr2Phi5x5;
   vector<float>   *Data_ph_clusterCellsLr1E7x11;
   vector<float>   *Data_ph_clusterCellsLr1Eta7x11;
   vector<float>   *Data_ph_clusterCellsLr1Phi7x11;
   vector<float>   *Data_ph_clusterCellsLr1E3x7;
   vector<float>   *Data_ph_clusterCellsLr1Eta3x7;
   vector<float>   *Data_ph_clusterCellsLr1Phi3x7;
   vector<float>   *Data_ph_clusterCellsLr1E5x5;
   vector<float>   *Data_ph_clusterCellsLr1Eta5x5;
   vector<float>   *Data_ph_clusterCellsLr1Phi5x5;
   vector<float>   *Data_ph_clusterCellsLr3E7x11;
   vector<float>   *Data_ph_clusterCellsLr3Eta7x11;
   vector<float>   *Data_ph_clusterCellsLr3Phi7x11;
   vector<float>   *Data_ph_clusterCellsLr3E3x7;
   vector<float>   *Data_ph_clusterCellsLr3Eta3x7;
   vector<float>   *Data_ph_clusterCellsLr3Phi3x7;
   vector<float>   *Data_ph_clusterCellsLr3E5x5;
   vector<float>   *Data_ph_clusterCellsLr3Eta5x5;
   vector<float>   *Data_ph_clusterCellsLr3Phi5x5;
   Double_t        Data_l1_Pt;
   Double_t        Data_l1_Eta;
   Double_t        Data_l1_Phi;
   Double_t        Data_l1_E;
   Double_t        Data_l1_Charge;
   Double_t        Data_l2_Pt;
   Double_t        Data_l2_Eta;
   Double_t        Data_l2_Phi;
   Double_t        Data_l2_E;
   Double_t        Data_l2_Charge;
   Double_t        Data_l1_SF;
   Double_t        Data_l2_SF;
   Double_t        Data_pu_wgt;
   Double_t        Data_Mu;
   Double_t        Data_mc_wgt;
   Double_t        Data_mc_xsec;
   Bool_t          Data_isZeey;
   Bool_t          Data_isZmumuy;
   vector<float>   *Data_M_ee;
   vector<float>   *Data_M_eey;
   vector<float>   *Data_M_mumu;
   vector<float>   *Data_M_mumuy;

   // List of branches
   TBranch        *b_Data_ph_Pt;   //!
   TBranch        *b_Data_ph_Eta;   //!
   TBranch        *b_Data_ph_Phi;   //!
   TBranch        *b_Data_ph_E;   //!
   TBranch        *b_Data_ph_topoetcone20;   //!
   TBranch        *b_Data_ph_topoetcone30;   //!
   TBranch        *b_Data_ph_topoetcone40;   //!
   TBranch        *b_Data_ph_ptcone20;   //!
   TBranch        *b_Data_ph_ptcone30;   //!
   TBranch        *b_Data_ph_ptcone40;   //!
   TBranch        *b_Data_ph_etcone20;   //!
   TBranch        *b_Data_ph_etcone30;   //!
   TBranch        *b_Data_ph_etcone40;   //!
   TBranch        *b_Data_ph_ptvarcone20;   //!
   TBranch        *b_Data_ph_ptvarcone30;   //!
   TBranch        *b_Data_ph_ptvarcone40;   //!
   TBranch        *b_Data_ph_isIsoLooseWP;   //!
   TBranch        *b_Data_ph_isIsoTightWP;   //!
   TBranch        *b_Data_ph_isIsoTightCaloOnlyWP;   //!
   TBranch        *b_Data_ph_Conversion;   //!
   TBranch        *b_Data_ph_Ethad;   //!
   TBranch        *b_Data_ph_Ethad1;   //!
   TBranch        *b_Data_ph_Rhad1;   //!
   TBranch        *b_Data_ph_Rhad;   //!
   TBranch        *b_Data_ph_E011;   //!
   TBranch        *b_Data_ph_E132;   //!
   TBranch        *b_Data_ph_E237;   //!
   TBranch        *b_Data_ph_E277;   //!
   TBranch        *b_Data_ph_Reta;   //!
   TBranch        *b_Data_ph_Rphi;   //!
   TBranch        *b_Data_ph_Weta1;   //!
   TBranch        *b_Data_ph_Weta2;   //!
   TBranch        *b_Data_ph_f1;   //!
   TBranch        *b_Data_ph_f3;   //!
   TBranch        *b_Data_ph_f3core;   //!
   TBranch        *b_Data_ph_fracs1;   //!
   TBranch        *b_Data_ph_Wstot1;   //!
   TBranch        *b_Data_ph_deltaE;   //!
   TBranch        *b_Data_ph_Eratio;   //!
   TBranch        *b_Data_ph_E2tsts1;   //!
   TBranch        *b_Data_ph_Emins1;   //!
   TBranch        *b_Data_ph_Emaxs1;   //!
   TBranch        *b_Data_ph_ClusterSize7x11Lr2;   //!
   TBranch        *b_Data_ph_ClusterSize3x7Lr2;   //!
   TBranch        *b_Data_ph_ClusterSize5x5Lr2;   //!
   TBranch        *b_Data_ph_ClusterSize7x11Lr3;   //!
   TBranch        *b_Data_ph_ClusterSize3x7Lr3;   //!
   TBranch        *b_Data_ph_ClusterSize5x5Lr3;   //!
   TBranch        *b_Data_ph_ClusterSize7x11Lr1;   //!
   TBranch        *b_Data_ph_ClusterSize3x7Lr1;   //!
   TBranch        *b_Data_ph_ClusterSize5x5Lr1;   //!
   TBranch        *b_Data_ph_clusterCellsLr2E7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Eta7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Phi7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr2E3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Eta3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Phi3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr2E5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Eta5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr2Phi5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr1E7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Eta7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Phi7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr1E3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Eta3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Phi3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr1E5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Eta5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr1Phi5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr3E7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Eta7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Phi7x11;   //!
   TBranch        *b_Data_ph_clusterCellsLr3E3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Eta3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Phi3x7;   //!
   TBranch        *b_Data_ph_clusterCellsLr3E5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Eta5x5;   //!
   TBranch        *b_Data_ph_clusterCellsLr3Phi5x5;   //!
   TBranch        *b_Data_l1_Pt;   //!
   TBranch        *b_Data_l1_Eta;   //!
   TBranch        *b_Data_l1_Phi;   //!
   TBranch        *b_Data_l1_E;   //!
   TBranch        *b_Data_l1_Charge;   //!
   TBranch        *b_Data_l2_Pt;   //!
   TBranch        *b_Data_l2_Eta;   //!
   TBranch        *b_Data_l2_Phi;   //!
   TBranch        *b_Data_l2_E;   //!
   TBranch        *b_Data_l2_Charge;   //!
   TBranch        *b_Data_l1_SF;   //!
   TBranch        *b_Data_l2_SF;   //!
   TBranch        *b_Data_pu_wgt;   //!
   TBranch        *b_Data_Mu;   //!
   TBranch        *b_Data_mc_wgt;   //!
   TBranch        *b_Data_mc_xsec;   //!
   TBranch        *b_Data_isZeey;   //!
   TBranch        *b_Data_isZmumuy;   //!
   TBranch        *b_Data_M_ee;   //!
   TBranch        *b_Data_M_eey;   //!
   TBranch        *b_Data_M_mumu;   //!
   TBranch        *b_Data_M_mumuy;   //!
   TBranch        *b_Data_i;

    TString sampletype;

void LinkDataBranches(TChain* fChain)
{
// Set object pointer
   Data_ph_clusterCellsLr2E7x11 = 0;
   Data_ph_clusterCellsLr2Eta7x11 = 0;
   Data_ph_clusterCellsLr2Phi7x11 = 0;
   Data_ph_clusterCellsLr2E3x7 = 0;
   Data_ph_clusterCellsLr2Eta3x7 = 0;
   Data_ph_clusterCellsLr2Phi3x7 = 0;
   Data_ph_clusterCellsLr2E5x5 = 0;
   Data_ph_clusterCellsLr2Eta5x5 = 0;
   Data_ph_clusterCellsLr2Phi5x5 = 0;
   Data_ph_clusterCellsLr1E7x11 = 0;
   Data_ph_clusterCellsLr1Eta7x11 = 0;
   Data_ph_clusterCellsLr1Phi7x11 = 0;
   Data_ph_clusterCellsLr1E3x7 = 0;
   Data_ph_clusterCellsLr1Eta3x7 = 0;
   Data_ph_clusterCellsLr1Phi3x7 = 0;
   Data_ph_clusterCellsLr1E5x5 = 0;
   Data_ph_clusterCellsLr1Eta5x5 = 0;
   Data_ph_clusterCellsLr1Phi5x5 = 0;
   Data_ph_clusterCellsLr3E7x11 = 0;
   Data_ph_clusterCellsLr3Eta7x11 = 0;
   Data_ph_clusterCellsLr3Phi7x11 = 0;
   Data_ph_clusterCellsLr3E3x7 = 0;
   Data_ph_clusterCellsLr3Eta3x7 = 0;
   Data_ph_clusterCellsLr3Phi3x7 = 0;
   Data_ph_clusterCellsLr3E5x5 = 0;
   Data_ph_clusterCellsLr3Eta5x5 = 0;
   Data_ph_clusterCellsLr3Phi5x5 = 0;
   Data_M_ee = 0;
   Data_M_eey = 0;
   Data_M_mumu = 0;
   Data_M_mumuy = 0;
   

   fChain->SetBranchAddress("ph_Pt", &Data_ph_Pt, &b_Data_ph_Pt);
   fChain->SetBranchAddress("ph_Eta", &Data_ph_Eta, &b_Data_ph_Eta);
   fChain->SetBranchAddress("ph_Phi", &Data_ph_Phi, &b_Data_ph_Phi);
   fChain->SetBranchAddress("ph_E", &Data_ph_E, &b_Data_ph_E);
   fChain->SetBranchAddress("ph_topoetcone20", &Data_ph_topoetcone20, &b_Data_ph_topoetcone20);
   fChain->SetBranchAddress("ph_topoetcone30", &Data_ph_topoetcone30, &b_Data_ph_topoetcone30);
   fChain->SetBranchAddress("ph_topoetcone40", &Data_ph_topoetcone40, &b_Data_ph_topoetcone40);
   fChain->SetBranchAddress("ph_ptcone20", &Data_ph_ptcone20, &b_Data_ph_ptcone20);
   fChain->SetBranchAddress("ph_ptcone30", &Data_ph_ptcone30, &b_Data_ph_ptcone30);
   fChain->SetBranchAddress("ph_ptcone40", &Data_ph_ptcone40, &b_Data_ph_ptcone40);
   fChain->SetBranchAddress("ph_etcone20", &Data_ph_etcone20, &b_Data_ph_etcone20);
   fChain->SetBranchAddress("ph_etcone30", &Data_ph_etcone30, &b_Data_ph_etcone30);
   fChain->SetBranchAddress("ph_etcone40", &Data_ph_etcone40, &b_Data_ph_etcone40);
   fChain->SetBranchAddress("ph_ptvarcone20", &Data_ph_ptvarcone20, &b_Data_ph_ptvarcone20);
   fChain->SetBranchAddress("ph_ptvarcone30", &Data_ph_ptvarcone30, &b_Data_ph_ptvarcone30);
   fChain->SetBranchAddress("ph_ptvarcone40", &Data_ph_ptvarcone40, &b_Data_ph_ptvarcone40);
   fChain->SetBranchAddress("ph_isIsoLooseWP", &Data_ph_isIsoLooseWP, &b_Data_ph_isIsoLooseWP);
   fChain->SetBranchAddress("ph_isIsoTightWP", &Data_ph_isIsoTightWP, &b_Data_ph_isIsoTightWP);
   fChain->SetBranchAddress("ph_isIsoTightCaloOnlyWP", &Data_ph_isIsoTightCaloOnlyWP, &b_Data_ph_isIsoTightCaloOnlyWP);
   fChain->SetBranchAddress("ph_Conversion", &Data_ph_Conversion, &b_Data_ph_Conversion);
   fChain->SetBranchAddress("ph_Ethad", &Data_ph_Ethad, &b_Data_ph_Ethad);
   fChain->SetBranchAddress("ph_Ethad1", &Data_ph_Ethad1, &b_Data_ph_Ethad1);
   fChain->SetBranchAddress("ph_Rhad1", &Data_ph_Rhad1, &b_Data_ph_Rhad1);
   fChain->SetBranchAddress("ph_Rhad", &Data_ph_Rhad, &b_Data_ph_Rhad);
   fChain->SetBranchAddress("ph_E011", &Data_ph_E011, &b_Data_ph_E011);
   fChain->SetBranchAddress("ph_E132", &Data_ph_E132, &b_Data_ph_E132);
   fChain->SetBranchAddress("ph_E237", &Data_ph_E237, &b_Data_ph_E237);
   fChain->SetBranchAddress("ph_E277", &Data_ph_E277, &b_Data_ph_E277);
   fChain->SetBranchAddress("ph_Reta", &Data_ph_Reta, &b_Data_ph_Reta);
   fChain->SetBranchAddress("ph_Rphi", &Data_ph_Rphi, &b_Data_ph_Rphi);
   fChain->SetBranchAddress("ph_Weta1", &Data_ph_Weta1, &b_Data_ph_Weta1);
   fChain->SetBranchAddress("ph_Weta2", &Data_ph_Weta2, &b_Data_ph_Weta2);
   fChain->SetBranchAddress("ph_f1", &Data_ph_f1, &b_Data_ph_f1);
   fChain->SetBranchAddress("ph_f3", &Data_ph_f3, &b_Data_ph_f3);
   fChain->SetBranchAddress("ph_f3core", &Data_ph_f3core, &b_Data_ph_f3core);
   fChain->SetBranchAddress("ph_fracs1", &Data_ph_fracs1, &b_Data_ph_fracs1);
   fChain->SetBranchAddress("ph_Wstot1", &Data_ph_Wstot1, &b_Data_ph_Wstot1);
   fChain->SetBranchAddress("ph_deltaE", &Data_ph_deltaE, &b_Data_ph_deltaE);
   fChain->SetBranchAddress("ph_Eratio", &Data_ph_Eratio, &b_Data_ph_Eratio);
   fChain->SetBranchAddress("ph_E2tsts1", &Data_ph_E2tsts1, &b_Data_ph_E2tsts1);
   fChain->SetBranchAddress("ph_Emins1", &Data_ph_Emins1, &b_Data_ph_Emins1);
   fChain->SetBranchAddress("ph_Emaxs1", &Data_ph_Emaxs1, &b_Data_ph_Emaxs1);
   fChain->SetBranchAddress("ph_ClusterSize7x11Lr2", &Data_ph_ClusterSize7x11Lr2, &b_Data_ph_ClusterSize7x11Lr2);
   fChain->SetBranchAddress("ph_ClusterSize3x7Lr2", &Data_ph_ClusterSize3x7Lr2, &b_Data_ph_ClusterSize3x7Lr2);
   fChain->SetBranchAddress("ph_ClusterSize5x5Lr2", &Data_ph_ClusterSize5x5Lr2, &b_Data_ph_ClusterSize5x5Lr2);
   fChain->SetBranchAddress("ph_ClusterSize7x11Lr3", &Data_ph_ClusterSize7x11Lr3, &b_Data_ph_ClusterSize7x11Lr3);
   fChain->SetBranchAddress("ph_ClusterSize3x7Lr3", &Data_ph_ClusterSize3x7Lr3, &b_Data_ph_ClusterSize3x7Lr3);
   fChain->SetBranchAddress("ph_ClusterSize5x5Lr3", &Data_ph_ClusterSize5x5Lr3, &b_Data_ph_ClusterSize5x5Lr3);
   fChain->SetBranchAddress("ph_ClusterSize7x11Lr1", &Data_ph_ClusterSize7x11Lr1, &b_Data_ph_ClusterSize7x11Lr1);
   fChain->SetBranchAddress("ph_ClusterSize3x7Lr1", &Data_ph_ClusterSize3x7Lr1, &b_Data_ph_ClusterSize3x7Lr1);
   fChain->SetBranchAddress("ph_ClusterSize5x5Lr1", &Data_ph_ClusterSize5x5Lr1, &b_Data_ph_ClusterSize5x5Lr1);
   fChain->SetBranchAddress("ph_clusterCellsLr2E7x11", &Data_ph_clusterCellsLr2E7x11, &b_Data_ph_clusterCellsLr2E7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr2Eta7x11", &Data_ph_clusterCellsLr2Eta7x11, &b_Data_ph_clusterCellsLr2Eta7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr2Phi7x11", &Data_ph_clusterCellsLr2Phi7x11, &b_Data_ph_clusterCellsLr2Phi7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr2E3x7", &Data_ph_clusterCellsLr2E3x7, &b_Data_ph_clusterCellsLr2E3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr2Eta3x7", &Data_ph_clusterCellsLr2Eta3x7, &b_Data_ph_clusterCellsLr2Eta3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr2Phi3x7", &Data_ph_clusterCellsLr2Phi3x7, &b_Data_ph_clusterCellsLr2Phi3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr2E5x5", &Data_ph_clusterCellsLr2E5x5, &b_Data_ph_clusterCellsLr2E5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr2Eta5x5", &Data_ph_clusterCellsLr2Eta5x5, &b_Data_ph_clusterCellsLr2Eta5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr2Phi5x5", &Data_ph_clusterCellsLr2Phi5x5, &b_Data_ph_clusterCellsLr2Phi5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr1E7x11", &Data_ph_clusterCellsLr1E7x11, &b_Data_ph_clusterCellsLr1E7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr1Eta7x11", &Data_ph_clusterCellsLr1Eta7x11, &b_Data_ph_clusterCellsLr1Eta7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr1Phi7x11", &Data_ph_clusterCellsLr1Phi7x11, &b_Data_ph_clusterCellsLr1Phi7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr1E3x7", &Data_ph_clusterCellsLr1E3x7, &b_Data_ph_clusterCellsLr1E3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr1Eta3x7", &Data_ph_clusterCellsLr1Eta3x7, &b_Data_ph_clusterCellsLr1Eta3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr1Phi3x7", &Data_ph_clusterCellsLr1Phi3x7, &b_Data_ph_clusterCellsLr1Phi3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr1E5x5", &Data_ph_clusterCellsLr1E5x5, &b_Data_ph_clusterCellsLr1E5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr1Eta5x5", &Data_ph_clusterCellsLr1Eta5x5, &b_Data_ph_clusterCellsLr1Eta5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr1Phi5x5", &Data_ph_clusterCellsLr1Phi5x5, &b_Data_ph_clusterCellsLr1Phi5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr3E7x11", &Data_ph_clusterCellsLr3E7x11, &b_Data_ph_clusterCellsLr3E7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr3Eta7x11", &Data_ph_clusterCellsLr3Eta7x11, &b_Data_ph_clusterCellsLr3Eta7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr3Phi7x11", &Data_ph_clusterCellsLr3Phi7x11, &b_Data_ph_clusterCellsLr3Phi7x11);
   fChain->SetBranchAddress("ph_clusterCellsLr3E3x7", &Data_ph_clusterCellsLr3E3x7, &b_Data_ph_clusterCellsLr3E3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr3Eta3x7", &Data_ph_clusterCellsLr3Eta3x7, &b_Data_ph_clusterCellsLr3Eta3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr3Phi3x7", &Data_ph_clusterCellsLr3Phi3x7, &b_Data_ph_clusterCellsLr3Phi3x7);
   fChain->SetBranchAddress("ph_clusterCellsLr3E5x5", &Data_ph_clusterCellsLr3E5x5, &b_Data_ph_clusterCellsLr3E5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr3Eta5x5", &Data_ph_clusterCellsLr3Eta5x5, &b_Data_ph_clusterCellsLr3Eta5x5);
   fChain->SetBranchAddress("ph_clusterCellsLr3Phi5x5", &Data_ph_clusterCellsLr3Phi5x5, &b_Data_ph_clusterCellsLr3Phi5x5);
   fChain->SetBranchAddress("l1_Pt", &Data_l1_Pt, &b_Data_l1_Pt);
   fChain->SetBranchAddress("l1_Eta", &Data_l1_Eta, &b_Data_l1_Eta);
   fChain->SetBranchAddress("l1_Phi", &Data_l1_Phi, &b_Data_l1_Phi);
   fChain->SetBranchAddress("l1_E", &Data_l1_E, &b_Data_l1_E);
   fChain->SetBranchAddress("l1_Charge", &Data_l1_Charge, &b_Data_l1_Charge);
   fChain->SetBranchAddress("l2_Pt", &Data_l2_Pt, &b_Data_l2_Pt);
   fChain->SetBranchAddress("l2_Eta", &Data_l2_Eta, &b_Data_l2_Eta);
   fChain->SetBranchAddress("l2_Phi", &Data_l2_Phi, &b_Data_l2_Phi);
   fChain->SetBranchAddress("l2_E", &Data_l2_E, &b_Data_l2_E);
   fChain->SetBranchAddress("l2_Charge", &Data_l2_Charge, &b_Data_l2_Charge);
   fChain->SetBranchAddress("l1_SF", &Data_l1_SF, &b_Data_l1_SF);
   fChain->SetBranchAddress("l2_SF", &Data_l2_SF, &b_Data_l2_SF);
   fChain->SetBranchAddress("pu_wgt", &Data_pu_wgt, &b_Data_pu_wgt);
   fChain->SetBranchAddress("Mu", &Data_Mu, &b_Data_Mu);
   fChain->SetBranchAddress("mc_wgt", &Data_mc_wgt, &b_Data_mc_wgt);
   fChain->SetBranchAddress("mc_xsec", &Data_mc_xsec, &b_Data_mc_xsec);
   fChain->SetBranchAddress("isZeey", &Data_isZeey, &b_Data_isZeey);
   fChain->SetBranchAddress("isZmumuy", &Data_isZmumuy, &b_Data_isZmumuy);
   fChain->SetBranchAddress("M_ee", &Data_M_ee, &b_Data_M_ee);
   fChain->SetBranchAddress("M_eey", &Data_M_eey, &b_Data_M_eey);
   fChain->SetBranchAddress("M_mumu", &Data_M_mumu, &b_Data_M_mumu);
   fChain->SetBranchAddress("M_mumuy", &Data_M_mumuy, &b_Data_M_mumuy);

}

TChain* LoadDataChain(vector<TString> names)
{
	vector<TString> Samples;
	for(int i = 0; i<names.size(); i++)
	{
	TString name = names.at(i);
	TSystemDirectory dir(name, name); 
	TList *files = dir.GetListOfFiles(); 
	if (files) { 
		TSystemFile *file; 
		TString fname; 
		TIter next(files); 
		while ((file=(TSystemFile*)next())) { 
			fname = file->GetName(); 
			if (!file->IsDirectory() && fname.EndsWith(".root") && fname.Contains("user.")) { 
				Samples.push_back(name+fname); 
			} 
		} 
	}
	}
	TChain *chain = new TChain("NTUP");;
	std::cout<<"The number of files will be loaded : "<< Samples.size() <<std::endl;
	if(Samples[0].Contains("data")){ sampletype = "Data" ;
	}else {sampletype = "MC" ;}

	std::cout<<"The samples are (MC or Data) : "<< sampletype <<std::endl;
	for(int i = 0; i<Samples.size(); i++)
	{
		std::cout<<Samples.at(i)<<std::endl;
		chain->AddFile(Samples.at(i),-1,"NTUP");	
	}
	
	 LinkDataBranches(chain);
	
	return chain;
}
